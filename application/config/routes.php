<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/*
  | -------------------------------------------------------------------------
  | URI ROUTING
  | -------------------------------------------------------------------------
  | This file lets you re-map URI requests to specific controller functions.
  |
  | Typically there is a one-to-one relationship between a URL string
  | and its corresponding controller class/method. The segments in a
  | URL normally follow this pattern:
  |
  |	example.com/class/method/id/
  |
  | In some instances, however, you may want to remap this relationship
  | so that a different class/function is called than the one
  | corresponding to the URL.
  |
  | Please see the user guide for complete details:
  |
  |	https://codeigniter.com/user_guide/general/routing.html
  |
  | -------------------------------------------------------------------------
  | RESERVED ROUTES
  | -------------------------------------------------------------------------
  |
  | There are three reserved routes:
  |
  |	$route['default_controller'] = 'welcome';
  |
  | This route indicates which controller class should be loaded if the
  | URI contains no data. In the above example, the "welcome" class
  | would be loaded.
  |
  |	$route['404_override'] = 'errors/page_missing';
  |
  | This route will tell the Router which controller/method to use if those
  | provided in the URL cannot be matched to a valid route.
  |
  |	$route['translate_uri_dashes'] = FALSE;
  |
  | This is not exactly a route, but allows you to automatically route
  | controller and method names that contain dashes. '-' isn't a valid
  | class or method name character, so it requires translation.
  | When you set this option to TRUE, it will replace ALL dashes in the
  | controller and method URI segments.
  |
  | Examples:	my-controller/index	-> my_controller/index
  |		my-controller/my-method	-> my_controller/my_method
 */
$route['default_controller'] = 'home/index';
$route['404_override'] = '';
$route['ajax-states'] = 'ajax/states';
$route['ajax-districts/'] = 'ajax/districts';
$route['ajax-districts/(:any)'] = 'ajax/districts/$1';
$route['ajax-blocks/(:any)'] = 'ajax/blocks/$1';
$route['ajax-villages/(:any)'] = 'ajax/village/$1';
$route['ajax-claim-under-village/(:any)'] = 'ajax/claimsInVillage/$1';
$route['ajax-claimant-under-village/(:any)'] = 'ajax/claimantsInVillage/$1';

$route['report-village-plot/(:any)'] = 'report/claim_village_plot/$1';
$route['report-village-plot-old'] = 'report/claim_village_plot';
$route['report-village-plot'] = 'report/claim_village_plot_new';

//$route['report-village-plot'] = 'report/claim_village_plot';
//$route['report-village-plot-new'] = 'report/claim_village_plot_new';

$route['ajax-get-plots-claim-village'] = 'ajax/get_claim_village_plots';
$route['ajax-get-plotlayout-old/(:any)'] = 'ajax/getPlotReportLayoutOld/$1';
$route['ajax-get-plotlayout/(:any)/(:any)'] = 'ajax/getPlotReportLayout/$1/$2';
$route['plotgpsGeoJSON/(:any)'] = 'ajax/getPlotGPSGeoJSON/$1';
$route['plotsatGeoJSON/(:any)'] = 'ajax/getPlotSATGeoJSON/$1';
$route['UpdatePlotGeometry/(:any)'] = 'ajax/updatePlotGeom/$1';

$route['plot-coordinates-info/(:any)/(:any)'] = 'ajax/getPlotCoordinateAndNo/$1/$2';

$route['ajax-shape-upload/(:any)'] = 'ajax/uploadshape/$1';

$route['sat-image/(:any)'] = 'satimagereport/sat_village_plot/$1';
$route['dumy/(:any)'] = 'satimagereport/sat_village_load/$1';

$route['translate_uri_dashes'] = FALSE;
