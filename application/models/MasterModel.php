<?php

class MasterModel extends CI_Model {

    //put your code here
    public function __construct() {
        parent::__construct();
    }

    public function insert_claim($data) {
        $this->db->insert('claim', $data);
        return $this->db->insert_id();
    }

    public function insert_claimant($data) {
        $this->db->insert('claimant', $data);
        return $this->db->insert_id();
    }

    public function insert_family($data) {
        $this->db->insert('family', $data);
        return $this->db->insert_id();
    }

    public function insert_plot($data) {
        $this->db->insert('plot', $data);
        return $this->db->insert_id();
    }

    public function insert_village($data) {
        $this->db->insert('village', $data);
        return $this->db->insert_id();
    }

    public function insert_block($data) {
        $this->db->insert('block', $data);
        return $this->db->insert_id();
    }

    public function insert_district($data) {
        $this->db->insert('district', $data);
        return $this->db->insert_id();
    }

    public function exec_query($query, $affRow = TRUE) {
        $res = $this->db->query($query);
        if ($affRow)
            return $this->db->affected_rows();
        else
            return $res;
    }

    public function get_states() {
        $this->db->select('stt_state_id as Id,stt_state_name Name')
                ->from('t_state')
                ->where("  stt_fl_archive='N' and is_rtp=TRUE ")
                ->order_by("stt_state_name");

        $res = $this->db->get();
        return $res;
    }

    public function get_districts($state_id = "") {
        $this->db->select('id as Id,name as Name')
                ->from('district')
                ->where("state_id='" . $state_id . "'")
                ->order_by('name');

        $res = $this->db->get();
        //echo $this->db->last_query();
        return $res;
    }

    public function get_blocks($state_id = "") {
        $this->db->select('id as Id,name as Name')
                ->from('block')
                ->where("dist_id='" . $state_id . "'")
                ->order_by('name');

        $res = $this->db->get();
        //echo $this->db->last_query();
        return $res;
    }

    public function get_villages($block_id = "") {
        $this->db->select('vil_id as Id,name as Name')
                ->from('village')
                ->where("block_id='" . $block_id . "' and status=1 and village.ogc_fid in( select distinct village_ogc_id from claimant)")
                ->order_by('name');

        $res = $this->db->get();
        //echo $this->db->last_query();
        return $res;
    }

    public function get_claims($village_id = "", $claimant_id = "") {
        if ($village_id && $claimant_id) {
            $this->db->select('id as Id,number as Name')
                    ->from('claim')
                    ->where("village_id='" . $village_id . "'")
                    ->where("claimant_id IN (" . $claimant_id . ")")
                    ->order_by('number');
        } else {
            $this->db->select('id as Id,number as Name')
                    ->from('claim')
                    ->where("village_id='" . $village_id . "'")
                    ->order_by('number');
        }

        $res = $this->db->get();
        //echo $this->db->last_query();
        return $res;
    }

    public function get_claimants($village_id = "") {
        $this->db->select('id as Id,old_id as Name')
                ->from('claimant')
                ->where("village_id='" . $village_id . "'");
        //->order_by('number');

        $res = $this->db->get();
        //echo $this->db->last_query();
        return $res;
    }

    public function insIssue($data) {
        $ins = $this->db->insert('t_local_issue', $data);

        if ($ins) {
            $lastId = $this->db->insert_id();
            return 'Last inserted ID: ' . $lastId;
        } else {
            return 'Error on insertion.';
        }
    }

    function get_village_old_id($village_id) {
        $this->db->select('old_id')
                ->from('village')
                ->where("vil_id='$village_id'");
        $res = $this->db->get();

        if ($res->num_rows() == 1) {
            $result = $res->result_array();
            $resp = $result[0]['old_id'];
        } else {
            $resp = FALSE;
        }
        return $resp;
    }

    function get_claim_old_ids($old_village_id) {
//        $old_village_id = '242003219';
        $sql = "SELECT array_to_string(array_agg(old_id),',') as claim_old_id FROM claim WHERE village = '$old_village_id'";

        $res = $this->db->query($sql);
        $result = $res->result_array();

        $resp = $result[0]['claim_old_id'];
        return $resp;
    }

    function get_plot_ids($village_id, $claimant_id_string = '', $claim_no = '',$claimstat='') {

        $sql = "SELECT array_to_string(array_agg(t3.ogc_fid ORDER BY t2.\"number\"::integer,t3.plt_number),',') AS plot_ids 
                FROM claimant t1 left join claim t2 on t1.id = t2.claimant_id  left join plot t3 on t2.id = t3.claim_id
				WHERE  t2.village_id='$village_id' and ST_IsEmpty(wkb_geometry) = false";
				//and t3.areagpssurveyh > 0 
        // echo $claimant_id_string;exit;
        if ($claimant_id_string) {
            $sql.= " AND  t1.id IN ($claimant_id_string)";
        }

        if ($claim_no) {
            $sql.= " AND  t2.id IN ($claim_no)";
        }
        if ($claimstat) {
            $sql.= " AND  t2.claim_status ='$claimstat'";
        }

        $res = $this->db->query($sql);
        $result = $res->result_array();
        $resp = $result[0]['plot_ids'];
		
//        echo $this->db->last_query();        die;
        return $resp;
    }

    function isGeomExist($plot_id) {
        $sql = "SELECT count(ogc_fid) as totNum FROM plot WHERE ogc_fid = $plot_id AND wkb_geometry IS NOT NULL";

        $res = $this->db->query($sql);
        $result = $res->result_array();

        $resp = $result[0]['totnum'];
        return $resp;
    }

    function getPlotOldId($plotId) {
        $sql = "SELECT old_id FROM plot WHERE ogc_fid = $plotId";
        $res = $this->db->query($sql);
        $result = $res->result_array();

        $resp = $result[0]['old_id'];
        return $resp;
    }
    function isPlotOld($plotId) {
        $sql = "SELECT is_old FROM plot WHERE ogc_fid = $plotId";
        $res = $this->db->query($sql);
        $result = $res->result_array();

        $resp = $result[0]['is_old'];
        return $resp;
    }

    function getAllPlotId() {
        $this->db->select('array_agg(ogc_fid)')
                ->from('plot')
                ->where("areagpssurveyh > 0");
        $res = $this->db->get();
        $result = $res->result_array();
        $ids = $result[0]['array_agg'];
        return $ids;
    }

    function getPlotDetailOld($plot_id) {
        $sql = "select plt_number,claim,areagpssurveyh,\"areaSat\",claim.\"number\" as claimnum,claimant.name as claimantName,claimant.old_id as claimantoldid,
            family.name as familymemname,family.relation as familymemrelation,pltvil.name as plotvillagename,resvil.name as resivillagename,
CASE
    WHEN plot.wkb_geometry_sat IS NULL THEN false
    ELSE true
  END 
  AS satvalue

from plot left join claim ON plot.claim = claim.old_id::integer left join claimant ON claimant.old_id = claim.claimant
left join family ON family.claimant_old = claimant.old_id
left join village as pltvil ON pltvil.old_id = plot.village::integer
left join village as resvil ON resvil.old_id = claimant.village::integer
where plot.ogc_fid=$plot_id";
        $res = $this->db->query($sql);
        return $res;
    }
    function _getPlotDetailNew($plot_id) {
        $sql = "select plt_number,claim,areagpssurveyh,\"areaSat\",claim.\"number\" as claimnum,claimant.name as claimantName,claimant.old_id as claimantoldid,
            family.name as familymemname,family.relation as familymemrelation,pltvil.name as plotvillagename,resvil.name as resivillagename,
CASE
    WHEN plot.wkb_geometry_sat IS NULL THEN false
    ELSE true
  END 
  AS satvalue

from plot left join claim ON plot.claim_id = claim.id left join claimant ON claimant.id = claim.claimant_id
left join family ON family.claimant_id = claimant.id
left join village as pltvil ON pltvil.vil_id = plot.vil_id
left join village as resvil ON resvil.vil_id = claimant.claimant_resi_village_id
where plot.ogc_fid=$plot_id";
        $res = $this->db->query($sql);
        return $res;
    }

	function getPlotDetailNew($plot_id) {
        $sql = "select plt_number,claim,areagpssurveyh,\"areaSat\",claim.\"number\" as claimnum,claimant.name as claimantName,claimant.old_id as claimantoldid,
            family.name as familymemname,family.relation as familymemrelation,pltvil.village_name as plotvillagename,resvil.village_name as resivillagename,
CASE
    WHEN plot.wkb_geometry_sat IS NULL THEN false
    ELSE true
  END 
  AS satvalue

from plot left join claim ON plot.claim_id = claim.id left join claimant ON claimant.id = claim.claimant_id
left join family ON family.claimant_id = claimant.id
left join t_village_master as pltvil ON pltvil.vil_id = plot.village_id_master
left join t_village_master as resvil ON resvil.vil_id = claimant.claimant_resi_master_village_id
where plot.ogc_fid=$plot_id";
        $res = $this->db->query($sql);
        return $res;
    }
}
