<?php

defined('BASEPATH') OR exit('No direct script access allowed');

function pre($args, $exit = false) {
    if ($exit) {
        echo '<pre>';
        print_r($args);
        echo '</pre>';
        die('Halt');
    } else {
        echo '<pre>';
        print_r($args);
        echo '</pre>';
    }
}
