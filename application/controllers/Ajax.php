<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Ajax extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->server = 'localhost';
        $this->mysqldb = 'landmapping_new';
        $this->myUser = 'root';
        $this->myPass = '';
        $this->load->helper('utility_helper');
        //$this->load->model('MasterModel');
    }

    public function index() {
        //$this->load->view('welcome_message');
    }

    public function states() {
        $result = $this->MasterModel->get_states();

        echo json_encode($result->result_array());
    }

    public function districts($state_id = "") {
        if(isset($_POST['state_id'])){$state_id = $_POST['state_id'];}
        if ($state_id == '')
            die('State not available');
        $result = $this->MasterModel->get_districts($state_id);
        //print_r($result->result_array());

        echo json_encode($result->result_array());
    }

    public function blocks($dist_id = "") {
        if ($dist_id == '')
            die('District not available');
        $result = $this->MasterModel->get_blocks($dist_id);
        //print_r($result->result_array());

        echo json_encode($result->result_array());
    }

    public function village($block_id = "") {
        if ($block_id == '')
            die('Block not available');
        $result = $this->MasterModel->get_villages($block_id);
        //print_r($result->result_array());

        echo json_encode($result->result_array());
    }

    public function get_claim_village_plots($village_id = "") {

        //send plot_id list as response
        $village_id = $this->input->post('vilid');
        $claimant_id_arr = $this->input->post('claimant_ids');
        $claim_no_arr = $this->input->post('claim_no');
        $claimstat = $this->input->post('claimstat');
        $claimant_id_arr = (!empty($claimant_id_arr)) ? array_filter($claimant_id_arr) : '';
        $claim_no_arr = (!empty($claim_no_arr)) ? array_filter($claim_no_arr) : '';
        $claimant_id_string = ($claimant_id_arr) ? "'" . implode("','", $claimant_id_arr) . "'" : '';

        $claim_no = ($claim_no_arr) ? implode(",", $claim_no_arr) : '';
        if ($village_id == '')
            die('Village not available');
        $plot_ids = $this->MasterModel->get_plot_ids($village_id, $claimant_id_string, $claim_no ,$claimstat);
        echo json_encode(array('plotIdList' => $plot_ids));
    }

    function claimsInVillage($village_id = "") {
        if ($village_id == '')
            die('Village not available');

        $claimant_id = $this->input->get('claimant_id');

        $result = $this->MasterModel->get_claims($village_id, $claimant_id);
        //print_r($result->result_array());

        echo json_encode($result->result_array());
    }

    function claimantsInVillage($village_id = "") {
        if ($village_id == '')
            die('Village not available');

        $result = $this->MasterModel->get_claimants($village_id);
        //print_r($result->result_array());

        echo json_encode($result->result_array());
    }

    function getPlotReportLayoutOld($plot_id) {
//        print_r($plot_id);        die;

        $data = array();
        $data['plotID'] = $plot_id;
        $data['villageName'] = $this->input->post('vilName');
        $data['stateName'] = $this->input->post('state');
        $data['distName'] = $this->input->post('district');
        $data['blockName'] = $this->input->post('block');

        $res = $this->MasterModel->getPlotDetailOld($plot_id);
        $result = $res->result_array();
        if (empty($result))
            echo json_encode(array('err' => 'Plot not found'));
        $data['pltDtl'] = $result[0];

        if ($result[0]['satvalue'] == 't') {
            $sql = "select ST_AsGeoJSON(wkb_geometry_sat) from plot where ogc_fid = $plot_id";
            $data['coord_type'] = 'SAT';
        } else {
            $sql = "select ST_AsGeoJSON(wkb_geometry) from plot where ogc_fid = $plot_id";
            $data['coord_type'] = 'GPS';
        }
        $res = $this->MasterModel->exec_query($sql, FALSE);
        $result = $res->result_array();
        $json = json_decode($result[0]['st_asgeojson'], TRUE);
        $data['coordinates'] = $json['coordinates'][0];
//        print_r( $data['pltDtl']);        die;

        $this->load->view('report/plot_layout', $data);
    }

    function getPlotReportLayout($plot_id, $option = 'false') {

        $data = array();
        $data['plotID'] = $plot_id;
        $data['old_plot_id'] = $this->MasterModel->getPlotOldId($plot_id);
        $data['is_old'] = $this->MasterModel->isPlotOld($plot_id);
        $data['villageName'] = $this->input->post('vilName');
        $data['stateName'] = $this->input->post('state');
        $data['distName'] = $this->input->post('district');
        $data['blockName'] = $this->input->post('block');

        if (empty($data['old_plot_id']) )//&& $data['is_old']!= TRUE
        {
            $res = $this->MasterModel->getPlotDetailNew($plot_id);
        }
        else
        {
            $res = $this->MasterModel->getPlotDetailOld($plot_id);
//            echo $this->db->last_query();
//            die;
        }

        $result = $res->result_array();
        if (empty($result))
            echo json_encode(array('err' => 'Plot not found'));
        $data['pltDtl'] = $result[0];


        $sql = "select ST_AsGeoJSON(wkb_geometry_sat) as wkb_geometry_sat ,ST_AsGeoJSON(wkb_geometry) as  wkb_geometry, "
                . "(st_area(ST_Transform(st_setsrid(wkb_geometry,4326),utmzone(ST_Centroid(st_setsrid(wkb_geometry,4326)))))/10000) As calc_area_hect_gps,"
                . "(st_area(ST_Transform(st_setsrid(wkb_geometry_sat,4326),utmzone(ST_Centroid(st_setsrid(wkb_geometry_sat,4326)))))/10000) As calc_area_hect_sat    from plot where ogc_fid = $plot_id";
        $res = $this->MasterModel->exec_query($sql, FALSE);
        $result = $res->result_array();
        //pre($result);
        $data['calc_area_hect_gps'] = $result[0]['calc_area_hect_gps'];
        $data['calc_area_hect_sat'] = $result[0]['calc_area_hect_sat'];
        $sat_cords_arr = array();
        $gps_cords_arr = array();
        $json_sat = json_decode($result[0]['wkb_geometry_sat']);
        $json_gps = json_decode($result[0]['wkb_geometry']);
        if (!empty($json_sat->coordinates[0][0])) {
            foreach ($json_sat->coordinates[0][0] as $key => $value) {
                array_push($sat_cords_arr, implode(',', $value));
            }
        }
        //pre($json_gps->coordinates[0]);exit;
        if (!empty($json_gps->coordinates[0][0])) {
            foreach ($json_gps->coordinates[0][0] as $key => $value) {
                array_push($gps_cords_arr, implode(',', $value));
            }
        }
        //pre($json_sat);
        $data['coordinates']['sat'] = json_encode($sat_cords_arr);

        $data['coordinates']['gps'] = json_encode($gps_cords_arr);

        //pre($data,true);
        if ($option == 'true') {
            echo json_encode($data);
        } else {
            $this->load->view('report/plot_layout', $data);
        }
    }

    function getPlotGPSGeoJSON($plotId) {

        if ($this->MasterModel->isGeomExist($plotId) == 1) {
            $sql = "SELECT row_to_json(fc) as geojson FROM ( SELECT 'FeatureCollection' As type, array_to_json(array_agg(f)) As features
                    FROM (SELECT 'Feature' As type, ST_AsGeoJSON(lg.wkb_geometry)::json As geometry
                    , row_to_json((ogc_fid, plt_number)) As properties,areagpssurveyh as area_gps FROM plot As lg WHERE ogc_fid = $plotId  ) As f )  As fc;";
            $res = $this->MasterModel->exec_query($sql, FALSE);
            $result = $res->result_array();
            //pre($result);
            print_r($result[0]['geojson']);
            //pre($result[0]['geojson']);
        } else {
            echo json_encode(array('status' => 'error'));
        }
    }

    function getPlotCoordinateAndNo($type = '', $plotId = '') {
        if ($type == '' || $plotId == '') {
            echo json_encode(array("status" => "invalid input"));
        }
        $response = array();
        if ($plotId) {
            $geometry_type = ($type == 'gps') ? 'wkb_geometry' : 'wkb_geometry_sat';
            $sql = " SELECT  ST_AsGeoJSON($geometry_type)::json As geometry,plt_number FROM plot WHERE ogc_fid = $plotId";
            $res = $this->MasterModel->exec_query($sql, FALSE);
            $result = $res->result_array();
            $response['geometry'] = $result[0]['geometry'];
            $response['plot_no'] = $result[0]['plt_number'];
            echo json_encode(array("status" => 'ok', 'response' => $response));
        }
    }

    function getPlotSATGeoJSON($plotId) {

        if ($this->MasterModel->isGeomExist($plotId) == 1) {
            $sql = "SELECT row_to_json(fc) as geojson FROM ( SELECT 'FeatureCollection' As type, array_to_json(array_agg(f)) As features
 FROM (SELECT 'Feature' As type, ST_AsGeoJSON(lg.wkb_geometry_sat)::json As geometry
    , row_to_json((ogc_fid, plt_number)) As properties,\"areaSat\" FROM plot As lg WHERE ogc_fid = $plotId  ) As f )  As fc;";
            $res = $this->MasterModel->exec_query($sql, FALSE);
            $result = $res->result_array();

            print_r($result[0]['geojson']);

            // pre($result[0]['geojson']);
        } else {
            echo json_encode(array('status' => 'error'));
        }
    }

    function plotCoordinates($plot_id = '') {
        $sql = "SELECT  ST_AsText(wkb_geometry) FROM plot where ogc_fid = " . $plot_id;
        $res = $this->MasterModel->exec_query($sql, FALSE);
        $result = $res->result_array();
        $resp = rtrim(ltrim($result[0]['st_astext'], 'POLYGON(('), '))');
        if (!empty($_GET['callback']))
            echo $_GET['callback'] . "(" . json_encode($resp) . ")";
        else
            echo $resp;
    }

}
