<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->server = 'localhost';
        $this->mysqldb = 'landmapping';
        $this->myUser = 'root';
        $this->myPass = 'liberty';
        $this->load->helper('utility_helper');
        die('not allowed');
    }

    public function index($param = '') {
        $migrations = array(
            "district" => base_url() . 'migration/district?salt=',
            "block" => base_url() . 'migration/block?salt=',
            "village" => base_url() . 'migration/village?salt=',
            "claimant" => base_url() . 'migration/claimant?salt=',
            "claim" => base_url() . 'migration/claim?salt=',
            "plot" => base_url() . 'migration/plot?salt=',
            "updatePlotGeometryGps" => base_url() . 'migration/UpdatePlotGeometry?salt=',
            "updaePlotGeometrySat" => base_url() . 'migration/UpdatePlotGeometrySatellite?salt=',
            "family" => base_url() . 'migration/family?salt=',
        );

        foreach ($migrations as $key => $value) {
            echo "<a href=\"$value\"  target=\"_blank\" >$key</a><br/>";
        }
    }

    public function district($plot_id = "") {
        $servername = 'localhost';
        $username = 'root';
        $password = 'liberty';

        try {
            $conn = new PDO("mysql:host=$servername;dbname=$this->mysqldb", $this->myUser, $this->myPass);
            // set the PDO error mode to exception
            $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

            $stmt = $conn->prepare("SELECT * FROM district");
            $stmt->execute();

            // set the resulting array to associative
            $result = $stmt->setFetchMode(PDO::FETCH_ASSOC);
            //pre($stmt->fetchAll(),true);exit;
            foreach ($stmt->fetchAll() as $k => $v) {
                $data = array(
                    'old_id' => $v['id'],
                    'code' => $v['code'],
                    'name' => $v['name'],
                    'state' => $v['state'],
                    'status' => $v['status'],
                    'code2001' => $v['code2001'],
                    'code2011' => $v['code2011']
                );
                echo '<br/>' . $this->MasterModel->insert_district($data);
            }
            $sql = "UPDATE district SET state_id=t_state.stt_state_id FROM t_state  WHERE t_state.code2011 = district.state";
            $this->MasterModel->exec_query($sql);

            $update_is_old_sql = "UPDATE district set is_old= true";
            $this->MasterModel->exec_query($update_is_old_sql);
        } catch (PDOException $e) {
            echo "Connection failed: " . $e->getMessage();
        }
    }

    public function block($plot_id = "") {

        try {
            $conn = new PDO("mysql:host=$this->server;dbname=$this->mysqldb", $this->myUser, $this->myPass);
            // set the PDO error mode to exception
            $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

            $stmt = $conn->prepare("SELECT * FROM tehsil");
            $stmt->execute();

            // set the resulting array to associative
            $result = $stmt->setFetchMode(PDO::FETCH_ASSOC);
            foreach ($stmt->fetchAll() as $k => $v) {
                $data = array(
                    'old_id' => $v['id'],
                    'code' => $v['code'],
                    'name' => $v['name'],
                    'district' => $v['district'],
                    'status' => $v['status'],
                    'code2001' => $v['code2001'],
                    'code2011' => $v['code2011']
                );
                echo '<br/>' . $this->MasterModel->insert_block($data);
            }
            $sql = "UPDATE block SET dist_id=district.id FROM district  WHERE block.district = district.old_id :: integer";
            $this->MasterModel->exec_query($sql);
            $update_is_old_sql = "UPDATE block set is_old= true";
            $this->MasterModel->exec_query($update_is_old_sql);
        } catch (PDOException $e) {
            echo "Connection failed: " . $e->getMessage();
        }
    }

    public function village($plot_id = "") {
        //geometry of village is available in mysql but not migrated to postgre sql
        try {
            $conn = new PDO("mysql:host=$this->server;dbname=$this->mysqldb", $this->myUser, $this->myPass);
            // set the PDO error mode to exception
            $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

            $stmt = $conn->prepare("SELECT * FROM village");
            $stmt->execute();
            // set the resulting array to associative
            $result = $stmt->setFetchMode(PDO::FETCH_ASSOC);

            foreach ($stmt->fetchAll() as $k => $v) {
                $data = array(
                    'old_id' => $v['id'],
                    'code' => $v['code'],
                    'name' => $v['name'],
                    'tehsil' => $v['tehsil'],
                    'status' => $v['status'],
                    'code2001' => $v['code2001'],
                    'code2011' => $v['code2011']
                );
                echo '<br/>' . $this->MasterModel->insert_village($data);

                //
            }
            $sql = "UPDATE village SET block_id = block.id  FROM block WHERE block.old_id = village.tehsil";
            $this->MasterModel->exec_query($sql);

            $update_is_old_sql = "UPDATE village set is_old= true";
            $this->MasterModel->exec_query($update_is_old_sql);
        } catch (PDOException $e) {
            echo "Connection failed: " . $e->getMessage();
        }
    }

    public function plot($claim_village_id = "") {
        ini_set('max_execution_time', 0);
        echo '<pre>';
        try {
            $conn = new PDO("mysql:host=$this->server;dbname=$this->mysqldb", $this->myUser, $this->myPass);
            // set the PDO error mode to exception
            $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

            $stmt = $conn->prepare("SELECT * FROM plot");
            $stmt->execute();

            // set the resulting array to associative
            $result = $stmt->setFetchMode(PDO::FETCH_ASSOC);
            //print_r($result)
            foreach ($stmt->fetchAll() as $k => $v) {
                $data = array(
                    'old_id' => $v['id'],
                    'claim' => $v['claim'],
                    'village' => $v['village'],
                    'plt_number' => $v['number'],
                    'surveyno' => $v['surveyno'],
                    'compartment_no' => $v['compartment_no'],
                    'areaclaimedh' => $v['areaclaimedh'],
                    'areaapprovedgsh' => $v['areaapprovedgsh'],
                    'areaapprovedsdlch' => $v['areaapprovedsdlch'],
                    'areaapproveddlch' => $v['areaapproveddlch'],
                    'areagpssurveyh' => $v['areagpssurveyh'],
                    'areaSat' => $v['areaSat'],
                    'is_common' => FALSE,
                );
                echo '<br/>' . $this->MasterModel->insert_plot($data);
//                $sql = "UPDATE village SET block_id = block.id  FROM block WHERE block.old_id = village.tehsil";
//                $this->MasterModel->exec_query($sql);
            }
            $update_claim_ogc_fld_sql = "UPDATE plot  SET claim_id   = claim.id  FROM claim WHERE claim.old_id::integer = plot.claim::integer";
            $this->MasterModel->exec_query($update_claim_ogc_fld_sql);

            $update_vil_id_sql = "UPDATE plot  SET vil_id   = claim.village_id  FROM claim WHERE claim.old_id::integer = plot.claim::integer";
            $this->MasterModel->exec_query($update_vil_id_sql);

            $update_is_old_sql = "UPDATE plot set is_old= true";
            $this->MasterModel->exec_query($update_is_old_sql);
        } catch (PDOException $e) {
            echo "Connection failed: " . $e->getMessage();
        }
        echo '</pre>';
    }

    public function claimant($claim_village_id = "") {
        ini_set('max_execution_time', 0);
        try {
            $conn = new PDO("mysql:host=$this->server;dbname=$this->mysqldb", $this->myUser, $this->myPass, array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'));
            // set the PDO error mode to exception
            $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

            $stmt = $conn->prepare("SELECT * FROM claimant");
            $stmt->execute();

            // set the resulting array to associative
            $result = $stmt->setFetchMode(PDO::FETCH_ASSOC);
            foreach ($stmt->fetchAll() as $k => $v) {
                $data = array(
                    'old_id' => $v['id'],
                    'name' => $v['name'],
                    'age' => $v['age'],
                    'voterid' => $v['voterid'],
                    'stofd' => $v['stofd'],
                    'village' => $v['village']
                );
                echo '<br/>' . $this->MasterModel->insert_claimant($data);
            }

            $sql = "UPDATE village SET block_id = block.id  FROM block WHERE block.old_id = village.tehsil";
            $this->MasterModel->exec_query($sql);

            $update_village_id_sql = "UPDATE claimant SET village_id = village.vil_id  FROM village WHERE village.old_id = claimant.village::integer";
            $this->MasterModel->exec_query($update_village_id_sql);

            $update_village_id_sql = "UPDATE claimant SET village_ogc_id = village.ogc_fid  FROM village WHERE village.old_id = claimant.village::integer";
            $this->MasterModel->exec_query($update_village_id_sql);

            $update_is_old_sql = "UPDATE claimant set is_old= true";
            $this->MasterModel->exec_query($update_is_old_sql);
        } catch (PDOException $e) {
            echo "Connection failed: " . $e->getMessage();
        }
    }

    public function family($claim_village_id = "") {

        try {
            $conn = new PDO("mysql:host=$this->server;dbname=$this->mysqldb", $this->myUser, $this->myPass, array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'));
            // set the PDO error mode to exception
            $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

            $stmt = $conn->prepare("SELECT * FROM family");
            $stmt->execute();

            // set the resulting array to associative
            $result = $stmt->setFetchMode(PDO::FETCH_ASSOC);
            foreach ($stmt->fetchAll() as $k => $v) {
                $data = array(
                    'old_id' => $v['id'],
                    'name' => $v['name'],
                    'age' => $v['age'],
                    'relation' => $v['relation'],
                    'claimant_old' => $v['claimant']
                );
                echo '<br/>' . $this->MasterModel->insert_family($data);
//                $sql = "UPDATE village SET block_id = block.id  FROM block WHERE block.old_id = village.tehsil";
//                $this->MasterModel->exec_query($sql);
            }
            $update_claimant_id_sql = "UPDATE family SET claimant_id = claimant.id  FROM claimant WHERE claimant.old_id = family.claimant_old";
            $this->MasterModel->exec_query($update_claimant_id_sql);

            $update_is_old_sql = "UPDATE family set is_old= true";
            $this->MasterModel->exec_query($update_is_old_sql);
        } catch (PDOException $e) {
            echo "Connection failed: " . $e->getMessage();
        }
    }

    public function claim($claim_village_id = "") {
        ini_set('max_execution_time', 0);
        try {
            $conn = new PDO("mysql:host=$this->server;dbname=$this->mysqldb", $this->myUser, $this->myPass);
            // set the PDO error mode to exception
            $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

            $stmt = $conn->prepare("SELECT * FROM claims");
            $stmt->execute();

            // set the resulting array to associative
            $result = $stmt->setFetchMode(PDO::FETCH_ASSOC);
            foreach ($stmt->fetchAll() as $k => $v) {
                $data = array(
                    'old_id' => $v['id'],
                    'number' => $v['number'],
                    'village' => $v['village'],
                    'claimant' => $v['claimant'],
                    'tareaclaimed' => $v['tareaclaimed'],
                    'gsdecision' => $v['gsdecision'],
                    'areaapprovedgsha' => $v['areaapprovedgsha'],
                    'sdlcdecision' => $v['sdlcdecision'],
                    'areaapprovedsdlcha' => $v['areaapprovedsdlcha'],
                    'dlcdecision' => $v['dlcdecision'],
                    'areaapproveddlcha' => $v['areaapproveddlcha'],
                    'appealfield' => $v['appealfield'],
                );
                echo '<br/>' . $this->MasterModel->insert_claim($data);
//                $sql = "UPDATE village SET block_id = block.id  FROM block WHERE block.old_id = village.tehsil";
//                $this->MasterModel->exec_query($sql);
            }
            $update_village_id_sql = "UPDATE claim SET village_id = village.vil_id  FROM village WHERE village.old_id = claim.village::integer";
            $this->MasterModel->exec_query($update_village_id_sql);

            $update_village_id_sql = "UPDATE claim SET claimant_id=claimant.id  FROM claimant  WHERE claim.claimant = claimant.old_id";
            $this->MasterModel->exec_query($update_village_id_sql);

            $update_is_old_sql = "UPDATE claim set is_old= true";
            $this->MasterModel->exec_query($update_is_old_sql);
        } catch (PDOException $e) {
            echo "Connection failed: " . $e->getMessage();
        }
    }

    public function UpdatePlotGeometry() {

        echo '<pre>';
        $startTime = microtime(true);
        $_SESSION['err_plot'] = array();
        $ids = $this->MasterModel->getAllPlotId();
        $plotIDS = explode(',', ltrim(rtrim($ids, '}'), '{'));
        //$plotIDS = array('16485');

        try {
            $conn = new PDO("mysql:host=$this->server;dbname=$this->mysqldb", $this->myUser, $this->myPass);
            // set the PDO error mode to exception
            $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            foreach ($plotIDS as $key => $plotId) {
                $oldPlotId = $this->MasterModel->getPlotOldId($plotId);
                if ($oldPlotId) {
                    $stmt = $conn->prepare("SELECT distinct CONCAT(`longitude`,' ', `latitude`) as LonLatString FROM `coordinate` WHERE `plot` = $oldPlotId ORDER BY vertex_no");
                    $stmt->execute();
                    // set the resulting array to associative
                    $result = $stmt->setFetchMode(PDO::FETCH_ASSOC);
                    //  pre($stmt->fetchAll());
                    $dataArr = $stmt->fetchAll();
                    $lat_lon_arr = array();
                    // pre($dataArr);exit;
                    end($dataArr);
                    $last_key = key($dataArr);

                    if (reset($dataArr) != end($dataArr)) {
                        $dataArr[$last_key + 1]['LonLatString'] = $dataArr[0]['LonLatString'];
                    }
                    foreach ($dataArr as $k => $value) {
                        array_push($lat_lon_arr, $value['LonLatString']);
                    }

                    $lat_lon_string = implode(',', $lat_lon_arr);

                    if ($lat_lon_string && count($lat_lon_arr) > 3) {
                        $sql = "UPDATE plot SET wkb_geometry=st_geomfromtext('MULTIPOLYGON(((" . $lat_lon_string . ")))',4326) WHERE ogc_fid = $plotId";
                    } else {
                        $sql = "UPDATE plot SET wkb_geometry=null  WHERE ogc_fid = $plotId";
                    }
                    $affRow = $this->MasterModel->exec_query($sql);
                    if ($affRow != 1) {//do stuffs if geometry does not updates properly
                        array_push($_SESSION['err_plot'], $oldPlotId);
                    }
                }
            }
            print_r($_SESSION['err_plot']);
            echo count($_SESSION['err_plot']) . '<br/>';
            echo $time_elapsed_secs = microtime(true) - $startTime;
        } catch (PDOException $e) {
            echo "Connection failed: " . $e->getMessage();
        }
    }

    //"SELECT * FROM updated_coordinate WHERE `plot` = $oldPlotId";
    public function UpdatePlotGeometrySatellite() {
        //die('satelite');
        echo '<pre>';
        $startTime = microtime(true);
        $_SESSION['err_plot'] = array();
        $ids = $this->MasterModel->getAllPlotId();
        $plotIDS = explode(',', ltrim(rtrim($ids, '}'), '{'));
        // $plotIDS = array('16485');

        try {
            $conn = new PDO("mysql:host=$this->server;dbname=$this->mysqldb", $this->myUser, $this->myPass);
            // set the PDO error mode to exception
            $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            foreach ($plotIDS as $key => $plotId) {
                $oldPlotId = $this->MasterModel->getPlotOldId($plotId);
                if ($oldPlotId) {
                    $stmt = $conn->prepare("SELECT distinct CONCAT(`longitude`,' ', `latitude`) as LonLatString FROM `updated_coordinate` WHERE `plot` = $oldPlotId ORDER BY vertex_no");
                    $stmt->execute();
                    // set the resulting array to associative
                    $result = $stmt->setFetchMode(PDO::FETCH_ASSOC);
                    //  pre($stmt->fetchAll());
                    $dataArr = $stmt->fetchAll();
                    $lat_lon_arr = array();

                    end($dataArr);
                    $last_key = key($dataArr);

                    if (reset($dataArr) != end($dataArr)) {
                        $dataArr[$last_key + 1]['LonLatString'] = $dataArr[0]['LonLatString'];
                    }
                    foreach ($dataArr as $k => $value) {
                        array_push($lat_lon_arr, $value['LonLatString']);
                    }
                    // pre($lat_lon_arr);exit;
                    $lat_lon_string = implode(',', $lat_lon_arr);
                    if ($lat_lon_string && count($lat_lon_arr) > 3) {
                        $sql = "UPDATE plot SET wkb_geometry_sat=st_geomfromtext('MULTIPOLYGON(((" . $lat_lon_string . ")))',4326) WHERE ogc_fid = $plotId";
                    } else {
                        $sql = "UPDATE plot SET wkb_geometry_sat=null  WHERE ogc_fid = $plotId";
                    }
                    $affRow = $this->MasterModel->exec_query($sql);
                    if ($affRow != 1) {//do stuffs if geometry does not updates properly
                        array_push($_SESSION['err_plot'], $oldPlotId);
                    }
                }
            }
            print_r($_SESSION['err_plot']);
            echo count($_SESSION['err_plot']) . '<br/>';
            echo $time_elapsed_secs = microtime(true) - $startTime;
        } catch (PDOException $e) {
            echo "Connection failed: " . $e->getMessage();
        }
    }

}

//$q = "select wkb_geometry from plot WHERE ogc_fid = $plotId";
//                    $wkb_geometry_sql = "SELECT 
//                                        CASE
//                                          WHEN  wkb_geometry is null  THEN false
//                                          ELSE true
//                                        END 
//                                        as exists  
//                                      from plot where ogc_fid = $plotId";
                    //$wkb_geometry_exists = $this->MasterModel->exec_query($sql);      