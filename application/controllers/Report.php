<?php
//session_start('Laravel');
defined('BASEPATH') OR exit('No direct script access allowed');

class Report extends CI_Controller {

    public function __construct() {
        parent::__construct();
      
    }

    public function claim_village_plot($plot_id = "") {
        $this->load->view('report/claim_village_plot');
    }

    public function claim_village_plot_new($plot_id = "") {
        $this->load->view('report/claim_village_plot_new');
    }

}
