<!doctype html>
<html class="no-js" lang="en">
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <meta name="robots" content="noindex">
        <title>Right To Property</title>
        <link rel="stylesheet" href="css/custom.css" />
        <link rel="stylesheet" href="css/bootstrap.css" />
        <link rel="stylesheet" href="css/font-awesome.css" />
        <script src="js/jquery.js"></script>
        <script src="js/bootstrap.js"></script>
        <script src="js/ol3/ol.js"></script>
        <script src="js/chosen.jquery.min.js"></script>
        <link rel="stylesheet" href="js/ol3/ol.css" />
        <link rel="stylesheet" href="js/chosen.min.css" />
    </head>
    <body >
        <div class="top_bar_container">
            <div class="container">
                <div class="row">
                    <h1>Welcome to home page</h1>
                </div>
            </div>
        </div>
        <div ><?php $this->load->view('common/nav'); ?></div>
        <div>
            Page Body
        </div>
        <!-- footer starts  -->
        <div class="footer_container">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="footer_content">
                            <div class="pull-left">Copyright &copy; Right To Property, All Rights Reserved.</div>
                        </div>

                        <div class="pull-right">
                            <div class="socialPart clearfix">
                                <a class="fb" href="javascript:void(0);"><i class="fa fa-facebook"></i></a>
                                <a class="tw" href="javascript:void(0);"><i class="fa fa-twitter"></i></a>

                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
