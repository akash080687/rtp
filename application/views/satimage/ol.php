<!doctype html>
<html>
    <head>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
        <link rel="stylesheet" href="https://openlayers.org/en/v4.1.1/css/ol.css" />
        <link rel="stylesheet" href="http://45.40.137.206:8008/mapguide/RTP/js/chosen.min.css" />

        <script src="https://openlayers.org/en/v4.1.1/build/ol.js"></script>
        <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    </head>
    <body>
        <!--        <div id="plot31189" class="" >
                    <div class=""></div>
                    <div id="map31189" class="map" style="margin: 5px;border: 1px solid red;height: 600px;"></div>
                    <div class=""></div>
                </div>-->
        <script type="text/javascript">
            var plotids = [29037, 31189, 31405, 31397];
            var currentPlotIndex = -1;
            var loadUptoIndex = 0;
            var binglayer = gpslayer = satlayer = WMSlayer = [];
            var map = [];
            function addmap(plotId) {
                map[plotId] = new ol.Map({
                    layers: [],
                    target: 'map' + plotId,
                    view: new ol.View({
//                    center: ol.proj.transform([73.77515422,21.55704821], 'EPSG:4326', 'EPSG:32643'),
                        center: [0, 0],
//                        projection: 'EPSG:4326',
                        zoom: 16
                    }),
                    controls: ol.control.defaults().extend([
                        new ol.control.ScaleLine({
                            units: 'metric'
                        })
                    ]),
                });
            }
            function addGpsLayer(plotId) {
                gpslayer[plotId] = new ol.layer.Vector({
                    name: 'PlotGPS',
                    stroke: new ol.style.Stroke({
                        color: [5, 225, 225, 0.5],
                        width: 2,
                        lineDash: [9, 4],
                        lineCap: 'round'
                    }),
                    source: new ol.source.Vector({
                        url: 'http://45.40.137.206:8008/mapguide/RTP/plotgpsGeoJSON/' + plotId,
                        format: new ol.format.GeoJSON({
                            defaultDataProjection: 'EPSG:4326',
//                            projection: 'EPSG:3857',
                        })
                    }),
                    zIndex: 3
                });
                map[plotId].addLayer(gpslayer[plotId])
            }
            function addSatLayer(plotId) {
                satlayer[plotId] = new ol.layer.Vector({
                    name: 'PlotSAT',
                    stroke: new ol.style.Stroke({
                        color: [5, 225, 225, 0.5],
                        width: 2,
                        lineDash: [9, 4],
                        lineCap: 'round'
                    }),
                    source: new ol.source.Vector({
                        url: 'http://45.40.137.206:8008/mapguide/RTP/plotsatGeoJSON/' + plotId,
                        format: new ol.format.GeoJSON({
                            defaultDataProjection: 'EPSG:4326',
                            projection: 'EPSG:3857',
                        })
                    }),
                    zIndex: 4
                });
                map[plotId].addLayer(satlayer[plotId])
            }
            function focus(plotId) {
                var extent = ol.extent.createEmpty();
                map[plotId].getLayers().forEach(function (layer) {
                    ol.extent.extend(extent, layer.getSource().getExtent());
                })
                map[plotId].getView().fit(extent);
            }
            function addBingLayer(plotId) {
                binglayer[plotId] = new ol.layer.Tile({
                    visible: true,
//                        preload: Infinity,
                    source: new ol.source.BingMaps({
                        key: 'Aq8wYwFzJ6vc0gaqMNZAHC42yBa1629gN5ybhNp0A-Ojzwvw4aAJUdhJHfcUfx9Y',
                        imagerySet: 'AerialWithLabels',
                        maxZoom: 19
                                // use maxZoom 19 to see stretched tiles instead of the BingMaps
                                // "no photos at this zoom level" tiles
                                // maxZoom: 19
                    }),
                    zIndex: 1
                })
                map[plotId].addLayer(binglayer[plotId]);
            }
            function addWMSlayer(plotId) {
map[plotId].getView().setZoom(16)
//        console.log(plotId);
//        console.log(ol.proj.transformExtent(map[plotId].getView().calculateExtent(map[plotId].getSize()),'EPSG:3857','EPSG:4326'));


                WMSlayer[plotId] = new ol.layer.Image({
//                        extent: window['plotSourceGPS' + plot_id].getExtent(),
        extent: map[plotId].getView().calculateExtent(map[plotId].getSize()), // window['plotSourceGPS' + plot_id].getExtent(),
//                    extent: ol.proj.transformExtent(map[plotId].getView().calculateExtent(map[plotId].getSize()), 'EPSG:3857', 'EPSG:4326'), // window['plotSourceGPS' + plot_id].getExtent(),
                    source: new ol.source.ImageWMS({
//                        url: encodeURI('http://localhost/cgi-bin/mapserv.exe?map=D:\\mapserver\\actualimage.map'),
                        url: encodeURI('http://localhost/cgi-bin/mapserv.exe?map=D:\\mapserver\\testmap.map'),
                        params: {
                            'LAYERS': 'Gujarat',
                            //                            'SRS': 'EPSG:32643',
                            //                            'BBOX': '361600.51891405135,2404154.8014285564,366487.01241288986,2409041.294927395',
                            //                            'WIDTH': '800',
                            //                            'HEIGHT': '500',
                            //                            'FORMAT': 'image/png'
                        },
                        serverType: 'mapserver',
                        //                        projection: ol.proj.get('EPSG:32643'),
                    }),
                    zIndex: 2
                })
//                source = WMSlayer.getSource();
//                source.on('imageloadstart', function() {
//                    alert('Load Start');
////        progress.addLoading();
//      });

//                key = WMSlayer[plotId].getSource().on('imageloadend', function () {
////                    alert('Load End');
////                    loadNextPlot(key);
//                });
                map[plotId].addLayer(WMSlayer[plotId]);


                //on wms load trigger next plot
            }
            function setZoom(plotId) {
                map[plotId].getView().setZoom(16);
            }
            function loadNextPlot() {
//                if (key != undefined)                    map[plotids[currentPlotIndex]].unByKey(key);
                currentPlotIndex++;
                if (plotids[currentPlotIndex] === undefined)
                    return;

                $('body').append('<div id="plot' + plotids[currentPlotIndex] + '" class="" ></div>');//summery



                $('#plot' + plotids[currentPlotIndex]).append('<div id="summery' + plotids[currentPlotIndex] + '" class="summerysec" ></div>');
                $('#plot' + plotids[currentPlotIndex]).append('<div id="map' + plotids[currentPlotIndex] + '" class="map" style="margin: 5px;border: 1px solid red;height: 600px;"></div>');
                $('#plot' + plotids[currentPlotIndex]).append('<div id="coordinate' + plotids[currentPlotIndex] + '" class="table-responsive coordinate" ></div>');
                $('body').append();//coordinates
                displayPlotMap(plotids[currentPlotIndex]);

            }
            function displayPlotMap(plotId) {
                addmap(plotId);
                addGpsLayer(plotId);
                gpslayer[plotId].on('change', function (event) {
                    focus(plotId);
                    addSatLayer(plotId);
//                    setZoom(plotId);
                    addBingLayer(plotId);
                    addWMSlayer(plotId);
                    loadNextPlot();
                })
            }
            loadNextPlot();
//            displayPlotMap(31189);


        </script>

    </body>
</html>