<!doctype html>
<html>
    <head>
        <link rel="stylesheet" href="http://127.0.0.1:8080/RTP/js/ol3/ol.css" />
        <link rel="stylesheet" href="http://127.0.0.1:8080/RTP/js/chosen.min.css" />

        <script src="http://127.0.0.1:8080/RTP/js/ol3/ol.js"></script>
        <script src="http://127.0.0.1:8080/RTP/js/jquery.js"></script>
    </head>
    <body>
        <div id="map" class="map" style="border: 1px solid red;height: 650px;"></div>


        <script>
//            var layers = [];lo

//            var layers = [
//                new ol.layer.Tile({
//                    source: new ol.source.OSM()
//                }),
//                new ol.layer.Image({
//                    extent: [361600.51891405135, 2404154.8014285564, 366487.01241288986, 2409041.294927395],
//                    source: new ol.source.ImageWMS({
//                        url: 'http://localhost/cgi-bin/mapserv.exe',
//                        params: {'LAYERS': 'Gujarat'},
//                        ratio: 1,
//                        serverType: 'mapserver'
//                    })
//                })
//            ];

//            var map = new ol.Map({
//                layers: layers,
//                target: 'map',
//                view: new ol.View({
//                    center: [361600.51891405135, 2404154.8014285564],
//                    zoom: 4
//                })
//            });
//
//            var layers_parcelle = new ol.layer.Image({
//                source: new ol.source.ImageWMS({
//                    url: 'http://192.168.1.11/cgi-bin/mapserv.exe?map=C:/ms4w/Apache/htdocs/Motors_importSIG/mapfile/toutes_les_parcelles.map',
//                    url : 'http://localhost/cgi-bin/mapserv.exe?map=D:\mapserver\actualimage.map',
//                            params: {
//                                // these are simply added to http-get parameter
//                                'LAYERS': 'Gujarat',
//                                'BBOX': '361600.51891405135,2404154.8014285564,366487.01241288986,2409041.294927395',
//                                'WIDTH': '800',
//                                'HEIGHT': '500',
//                                'TRANSPARENT': 'true'
//                            },
//                })
//            });
//            map.addLayer(layers_parcelle);
        </script>

        <script type="text/javascript">
            var imageLayer;
            var plot_id = <?= $this->uri->segment(2) ?>;//;31519;
            var gpsurl = 'http://127.0.0.1:8080/RTP/plotgpsGeoJSON/' + plot_id;// need to eliminate server side junk code
//            var gpsurl = 'https://openlayers.org/en/v4.1.1/examples/data/geojson/countries.geojson';// need to eliminate server side junk code
//            var saturl = 'http://45.40.137.206:8008/mapguide/RTP/plotsatGeoJSON/' + plot_id;
            window['plotSourceGPS' + plot_id] = new ol.source.Vector({
                url: gpsurl,
                format: new ol.format.GeoJSON({
                    defaultDataProjection: 'EPSG:4326',
                    projection: 'EPSG:3857',
                }),
            });

//            window['plotSourceGPS' + plot_id].on('tileloadend', function(event) {
//  alert('Hi...');
//});

            window['plotSourceGPS' + plot_id].on('change', function (evt) {
                if (window['plotSourceGPS' + plot_id].getState() === 'ready') {
//        alert(window['plotSourceGPS' + plot_id].getState());

//                    imageLayer = new ol.layer.Image({
////                        extent: window['plotSourceGPS' + plot_id].getExtent(),
//                        extent: map.getView().calculateExtent(map.getSize()),// window['plotSourceGPS' + plot_id].getExtent(),
//                        source: new ol.source.ImageWMS({
//                            url: encodeURI('http://localhost/cgi-bin/mapserv.exe?map=D:\\mapserver\\actualimage.map'),
//                            params: {
//                                'LAYERS': 'Gujarat',
//                                //                            'SRS': 'EPSG:32643',
//                                //                            'BBOX': '361600.51891405135,2404154.8014285564,366487.01241288986,2409041.294927395',
//                                //                            'WIDTH': '800',
//                                //                            'HEIGHT': '500',
//                                //                            'FORMAT': 'image/png'
//                            },
//                            serverType: 'mapserver',
//                            //                        projection: ol.proj.get('EPSG:32643'),
//                        })
//                    })
//
//                    map.addLayer(imageLayer);
                }
            });


            window['plotGPS' + plot_id] = new ol.layer.Vector({
                name: 'PlotGPS',
                style: new ol.style.Style({
                    fill: new ol.style.Fill({
                        color: [203, 194, 185, 0]
                    }),
                    stroke: new ol.style.Stroke({
                        color: [5, 225, 225, 0.5],
                        width: 2,
                        lineDash: [9, 4],
                        lineCap: 'round'
                    }),
//                    zIndex: 2
                }),
                zIndex: 2
            });

            window['plotGPS' + plot_id].setSource(window['plotSourceGPS' + plot_id]);
            var projection = new ol.proj.Projection({
                code: 'EPSG:3857',
                units: 'm'
            });
            var layers = [
                /* new ol.layer.Image({
                 extent: [8212481.663640973, 2458339.4519081176, 8212694.645656734, 2458457.5634519514],
                 source: new ol.source.ImageWMS({
                 url: encodeURI('http://localhost/cgi-bin/mapserv.exe?map=D:\\mapserver\\actualimage.map'),
                 params: {
                 'LAYERS': 'Gujarat',
                 //                            'SRS': 'EPSG:32643',
                 //                            'BBOX': '361600.51891405135,2404154.8014285564,366487.01241288986,2409041.294927395',
                 //                            'WIDTH': '800',
                 //                            'HEIGHT': '500',
                 //                            'FORMAT': 'image/png'
                 },
                 serverType: 'mapserver',
                 //                        projection: ol.proj.get('EPSG:32643'),
                 })
                 }),*/

            ];
            var map = new ol.Map({
                layers: layers,
                target: 'map',
                view: new ol.View({
//                    center: ol.proj.transform([73.77515422,21.55704821], 'EPSG:4326', 'EPSG:32643'),
                    center: [0, 0],
//                    projection: projection,
                    zoom: 17
                }),
                controls: ol.control.defaults().extend([
                    new ol.control.ScaleLine({
                        units : 'metric'
                    })
                ]),
            });
//             window['map' + plot_id].addLayer(window['plotGPS' + plot_id]);
            window['map'].addLayer(window['plotGPS' + plot_id]);
            window['plotSourceGPS' + plot_id].on('change', function (event) {
                window['map'].getView().fit(window['plotSourceGPS' + plot_id].getExtent(), window['map'].getSize())
            });


map.once('postrender', function(event) {
    addwms();
});

function addwms(){
    imageLayer = new ol.layer.Image({
//                        extent: window['plotSourceGPS' + plot_id].getExtent(),
                        extent: map.getView().calculateExtent(map.getSize()),// window['plotSourceGPS' + plot_id].getExtent(),
                        source: new ol.source.ImageWMS({
                            url: encodeURI('http://localhost/cgi-bin/mapserv.exe?map=D:\\mapserver\\actualimage.map'),
                            params: {
                                'LAYERS': 'Gujarat',
                                //                            'SRS': 'EPSG:32643',
                                //                            'BBOX': '361600.51891405135,2404154.8014285564,366487.01241288986,2409041.294927395',
                                //                            'WIDTH': '800',
                                //                            'HEIGHT': '500',
                                //                            'FORMAT': 'image/png'
                            },
                            serverType: 'mapserver',
                            //                        projection: ol.proj.get('EPSG:32643'),
                        })
                    })

                    map.addLayer(imageLayer);
}
// http://45.40.137.206:8008/mapguide/RTP/sat-image/31189
// http://45.40.137.206:8008/mapguide/RTP/sat-image/31405
// http://45.40.137.206:8008/mapguide/RTP/sat-image/31397

        </script>

    </body>
</html>