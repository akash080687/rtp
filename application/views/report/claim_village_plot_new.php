<!doctype html>
<html class="no-js" lang="en">
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <meta name="robots" content="noindex">
        <title>Right To Property</title>
        <link rel="stylesheet" href="<?php echo base_url(); ?>css/custom.css?<?=  uniqid()?>" />
        <link rel="stylesheet" href="<?php echo base_url(); ?>css/bootstrap.css" />
        <link rel="stylesheet" href="<?php echo base_url(); ?>css/font-awesome.css" />
        <script src="<?php echo base_url(); ?>js/jquery.js"></script>
        <script src="<?php echo base_url(); ?>js/bootstrap.js"></script>
         <!--<script src="js/angular.min.js"></script>-->
        <!--<script src="js/angular-route.min.js"></script>-->
        <script src="<?php echo base_url(); ?>js/ol3/ol.js"></script>
        <script src="<?php echo base_url(); ?>js/chosen.jquery.min.js"></script>
        <link rel="stylesheet" href="<?php echo base_url(); ?>js/ol3/ol.css" />
        <link rel="stylesheet" href="<?php echo base_url(); ?>js/chosen.min.css" />
        <style>

            @media print{
                .ol-control{display:block !important;}
                .ol-zoom.ol-unselectable.ol-control {display:none !important;}

                .ol-rotate.ol-hidden {
                    opacity: 1 !important;
                    visibility: visible !important;
                    transition: all .5s ease-in-out !important;
                }

            }

        </style>
        <script>
            var ip = '<?php          echo $_SERVER['REMOTE_ADDR'];         ?>';
            var strokeClr = 'magenta';
            var fillClr = [255, 255, 255, .25];
//            var map;
//            var plotSourceGPS;
            function createPolygonStyleFunction(feature, resolution) {               
                var styles = [new ol.style.Style({
                    stroke: new ol.style.Stroke({
                        color: strokeClr,
                        width: 1.5,
                        lineDash: [5, 4]
                    }),
//                    fill: new ol.style.Fill({
//                        color: fillClr
//                    }),
                    text: createTextStyle(feature, resolution)
                })];
            
              if (feature.getGeometry().getType() === 'MultiPolygon' && !plotDetail[feature.getProperties().f1].sat) {
                    var geometry = new ol.geom.LineString(feature.getGeometry().getCoordinates()[0][0].slice(0, 3));
                    geometry.forEachSegment(function (start, end) {
                        var dx = end[0] - start[0];
                        var dy = end[1] - start[1];
                        var rotation = Math.atan2(dy, dx);

                        // arrows
                        styles.push(new ol.style.Style({
                            geometry: new ol.geom.Point(end),
                            image: new ol.style.Icon({
                                src: 'http://127.0.0.1:8080/RTP/images/dotarrow.png',
                                anchor: [0.75, 0.5],
                                rotateWithView: false,
                                rotation: -rotation
                            })
                        }));
                        // arrows
                    });
//                    styles.push(new ol.style.Style({
//                        geometry: geometryPoint,
//                        image: new ol.style.Circle({
//                            radius: 3,
//                            fill: new ol.style.Fill({color: 'red'})
//
//                        })
//
//                    }))
//                    return styles;
                }
            return styles;
//                return [style];
            }

            function createPointStyleFunction(feature, resolution) {
                var fill,rad;
                if(feature.get('name')=='point0'){fill='#ff00ff';rad=7}else{fill='#ffffff';rad=3}
                var style = new ol.style.Style({
                    image: new ol.style.Circle({
                        radius: rad,
                        fill: new ol.style.Fill({color: fill }),
                        stroke: new ol.style.Stroke({color: '#ff00ff', width: 1})
                    })
                });
                return [style];
            }
//            var geometryPoint;
//var  geometry;
//var  feat;
            function createPolygonStyleFunctionSat(feature, resolution) {
                feat = feature;
                var styles = [new ol.style.Style({
                        stroke: new ol.style.Stroke({
                            color: strokeClr,
                            width: 2,
                        }),
//                    fill: new ol.style.Fill({
//                        color: fillClr
//                    }),
//                        text: createTextStyleSat(feature, resolution)
                    })];

                if (feature.getGeometry().getType() === 'MultiPolygon') {
                    var geometry = new ol.geom.LineString(feature.getGeometry().getCoordinates()[0][0].slice(0, 3));
                    geometry.forEachSegment(function (start, end) {
                        var dx = end[0] - start[0];
                        var dy = end[1] - start[1];
                        var rotation = Math.atan2(dy, dx);

                        // arrows
                        styles.push(new ol.style.Style({
                            geometry: new ol.geom.Point(end),
                            image: new ol.style.Icon({
                                src: 'http://127.0.0.1:8080/RTP/images/arrow_magenta.png',
                                anchor: [0.75, 0.5],
                                rotateWithView: false,
                                rotation: -rotation
                            })
                        }));
                        // arrows
                    });
//                    styles.push(new ol.style.Style({
//                        geometry: geometryPoint,
//                        image: new ol.style.Circle({
//                            radius: 3,
//                            fill: new ol.style.Fill({color: 'red'})
//
//                        })
//
//                    }))
                    return styles;
                }
            }

            var createTextStyle = function (feature, resolution) {
                if (feature.get('layer') == 'pb')
                    var txt = resolution < 50 ? feature.get('f1') : '';
                else
                    var txt = resolution < 500 ? feature.get('f2') : '';
                return new ol.style.Text({
                    textAlign: 'center',
                    textBaseline: 'bottom',
                    font: '16px Verdana',
                    text: txt,
                    fill: new ol.style.Fill({color: 'red'}),
                    offsetY: 20,
                    stroke: new ol.style.Stroke({color: 'white', width: 2})
                });
            }
            var createTextStyleSat = function (feature, resolution) {
                if (feature.get('layer') == 'pb')
                    var txt = resolution < 50 ? feature.get('f1') : '';
                else
                    var txt = resolution < 500 ? feature.get('f2') : '';
                return new ol.style.Text({
                    textAlign: 'center',
                    textBaseline: 'bottom',
                    font: 'bold 20px Verdana',
                    text: txt,
                    fill: new ol.style.Fill({color: 'red'}),
                    offsetY: 20,
                    stroke: new ol.style.Stroke({color: 'white', width: 2})
                });
            }
        </script>
    </head>
    <body >        
        <!-- top bar starts -->
        <div class="top_bar_container">
            <div class="container">
                <div class="row">
                    <div class="top_bar">
                        <div class="col-sm-12">
							<div class="hderTableCvr">
								<div class="logo"><a href="javascript:void(0);"><img src="<?php echo base_url(); ?>images/logo.png" alt="" /></a></div>
								<h1 class="logo_text_left">Right to property</h1>
								<div class="logo_text_right">
									<div class="logo_text_right_top">Mapping land, documenting, evidence, and claiming title under the Forest Rights Act</div>
									<div class="logo_text_right_bottom">An initiative of ARCH Vahini, Gujarat and Liberty Institute, New Delhi.  </div>
								</div>
							</div>	
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div ><?php $this->load->view('common/nav'); ?></div>
        <div><?php $this->load->view('report/viewPlotNew.php'); ?></div>


        <!-- footer starts  -->
        <div class="footer_container">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="footer_content">
                            <div class="pull-left">Copyright &copy; Right To Property, All Rights Reserved.</div>
                        </div>

                        <div class="pull-right">
                            <div class="socialPart clearfix">
                                <a class="fb" href="javascript:void(0);"><i class="fa fa-facebook"></i></a>
                                <a class="tw" href="javascript:void(0);"><i class="fa fa-twitter"></i></a>

                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <script>
            var serviceUrl = 'http://<?php echo $_SERVER['HTTP_HOST']?>'+'/';
        </script>

        <div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
            <div class="modal-dialog modal-sm">
                <div  class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="gridSystemModalLabel">Add Village to the current block</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row text-center">
                            <input type="text" name="" id="" placeholder="Village Name ..."/>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary" onclick="$('#block').val() ? alert('Ok') : alert('Please select Block')">Save changes</button>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade bs-example-modal-sm2" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
            <div class="modal-dialog modal-sm">
                <div  class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="gridSystemModalLabel"> Village code</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row text-center">
                            <input type="text" name="" id="vilcode" placeholder="Village Code Not available"/>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
                <!-- <script src="js/app.js"></script> -->

    </body>
</html>
