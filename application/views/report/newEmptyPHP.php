<?php

$sql = "SELECT row_to_json(fc) as geojson  FROM ( SELECT 'FeatureCollection' As type, array_to_json(array_agg(f)) As features
  FROM (SELECT 'Feature' As type, ST_AsGeoJSON(lg.wkb_geometry)::json As geometry, row_to_json(lp) As properties
  FROM ac_boundary As lg   INNER JOIN (SELECT ac_no as id, ac_name as name,'ac' as layer FROM ac_boundary";
$sql = $sql . " and id='$acId'";

$sql = $sql . ") As lp  ON lg.ac_no = lp.id  ) As f )  As fc;";
$query = $this->db->query($sql);
$result = $query->result_array();

echo $result[0]['geojson'];
