
<!-- breadcrumb ends  -->
<script>
    var laravelBaseUrl = 'http://righttoproperty.local/';
    var localBaseUrl = 'http://rtp.local/';
    var laravelCSRFUrl = laravelBaseUrl + 'ajax/get-csrf';
    var CSRFLaravel = '';
    function populateDistrict(state_id) {
        $('.display_panel_content').css('cursor', 'wait');
        $('#district').children(':not(:first-child)').remove();
//        $.get(serviceUrl + 'ajax-districts/' + state_id, function (data) {
        $.post(laravelBaseUrl + 'ajax/districts', {state_id: state_id, _token: CSRFLaravel}, function (data) {
            $.pos
            $.each(data, function (index, value) {
                $('#district').append('<option  value="' + $(this)[0].Id + '" >' + $(this)[0].Name + '</option>')
            });
//            $('#district').val('3');
        }, 'json').done(function () {
            $('.display_panel_content').css('cursor', '');
        });
        //$('#district').css('cursor', '');
    }

    function populateBlock(district_id) {
        $('.display_panel_content').css('cursor', 'wait');
        $('#block').children(':not(:first-child)').remove();
//        $.get(serviceUrl + 'ajax-blocks/' + district_id, function (data) {
        $.post(laravelBaseUrl + 'ajax/blocks', {dist_id: district_id, _token: CSRFLaravel}, function (data) {
            $.each(data, function (index, value) {
                $('#block').append('<option  value="' + $(this)[0].Id + '" >' + $(this)[0].Name + '</option>')
            });
//             $('#block').val();
        }, 'json').done(function () {
            $('.display_panel_content').css('cursor', '');
        });

    }
    function populateVillage(block_id) {
        $('.display_panel_content').css('cursor', 'wait');
        $('#village').children(':not(:first-child)').remove();
//        $.get(serviceUrl + 'ajax-villages/' + block_id, function (data) {
        $.post(laravelBaseUrl + 'ajax/villages', {block_id: block_id, _token: CSRFLaravel}, function (data) {
            $.each(data, function (index, value) {
                $('#village').append('<option  value="' + $(this)[0].Id + '" >' + $(this)[0].Name + '</option>')
            });
        }, 'json').done(function () {
            $('.display_panel_content').css('cursor', '');
        });
    }
    function populateMohalla(village_id) {
        $('.display_panel_content').css('cursor', 'wait');
        $('#mohalla').children(':not(:first-child)').remove();
//        $.get(serviceUrl + 'ajax-villages/' + block_id, function (data) {
        $.post(laravelBaseUrl + 'ajax/mohollas', {village_id: village_id, _token: CSRFLaravel}, function (data) {
            $.each(data, function (index, value) {
                $('#mohalla').append('<option  value="' + $(this)[0].Id + '" >' + $(this)[0].Name + '</option>')
            });
        }, 'json').done(function () {
            $('.display_panel_content').css('cursor', '');
        });
    }
    function populateClaims(village_id, moholla_id, claimant_id) {
        $('.display_panel_content').css('cursor', 'wait');
        //$('#claim').children(':not(:first-child)').remove();
        //alert(village_id);
        //$.get(serviceUrl + 'ajax-claim-under-village/' + village_id + '?claimant_id=' + claimant_id, function (data) {
        $.post(laravelBaseUrl + 'ajax/claims-by-village', {village_id: village_id, claimant_id: claimant_id, moholla_id: moholla_id, _token: CSRFLaravel}, function (data) {
            $.each(data, function (index, value) {
                $('#claim').append('<option  value="' + $(this)[0].Id + '" >' + $(this)[0].Name + '</option>');
            });
            setTimeout(function () {
                $(".chosen-select").chosen();
            }, 500);
        }, 'json').done(function () {
            $('.display_panel_content').css('cursor', '');
        });
    }
    function populateClaimant(village_id, moholla_id) {
        $('#claimant').css('cursor', 'wait');
        $('#claimant').children(':not(:first-child)').remove();

        //$.get(serviceUrl + 'ajax-claimant-under-village/' + village_id, function (data) {
        $.post(laravelBaseUrl + 'ajax/claimants-by-village', {village_id: village_id, _token: CSRFLaravel}, function (data) {
            $.each(data, function (index, value) {
                $('#claimant').append('<option   value="' + $(this)[0].Id + '" >' + $(this)[0].Name + '</option>');
            });
            setTimeout(function () {
                $(".chosen-select").chosen();
            }, 500);
        }, 'json').done(function () {
            $('.display_panel_content').css('cursor', '');
        });
        //choosen         

        //choosen 
    }
    function populateClaimantByMoholla(village_id, moholla_id) {
        $('#claimant').css('cursor', 'wait');
        $('#claimant').children(':not(:first-child)').remove();

        //$.get(serviceUrl + 'ajax-claimant-under-village/' + village_id, function (data) {
        $.post(laravelBaseUrl + 'ajax/claimants-by-moholla', {moholla_id: moholla_id, _token: CSRFLaravel}, function (data) {
            $.each(data, function (index, value) {
                $('#claimant').append('<option  onchange="populateClaims(' + village_id + ',' + moholla_id + ',' + $(this)[0].Id + ')" value="' + $(this)[0].Id + '" >' + $(this)[0].Name + '</option>');
            });
            setTimeout(function () {
                $(".chosen-select").chosen();
            }, 500);
        }, 'json').done(function () {
            $('.display_panel_content').css('cursor', '');
        });
        //choosen         

        //choosen 
    }

    function convert_hectar_to_acre(value) {
        return (value * 2.4710538).toFixed(2);
    }

    function convert_acre_to_hectar(value) {
        return (value * 0.40468564224).toFixed(2);
    }



    var plotIds = null;
    var plotDetail = [];
    var plotIdLen = 0;
    var curIndex = 0;
    var loadcount = 0;
    var loadUntil = 0;
    var plotCoords = null;
    var claimant_ids = [];
    var claim_no = [];
    var last_loded_plot_index = 0;
    var stop_load = false;
    var total_loaded_plots = 0;

    function load_historical_image(lastIndexPlotImage) {
        if (lastIndexPlotImage < last_loded_plot_index)
        {
//            console.log(plotIds[lastIndexPlotImage])
            addWMSlayer(plotIds[lastIndexPlotImage])
            if (lastIndexPlotImage % 5 == 0)
                setTimeout(function (){
                    load_historical_image(++lastIndexPlotImage)

                            
                }, 5000)
            else
                load_historical_image(++lastIndexPlotImage)
        } else {
            console.log('Historical Image load ended');
                            // var parent_id = "plot" + plotIds[lastIndexPlotImage];
                            // var plot_parent_id = "#" + parent_id;

                            // console.log(plot_parent_id);
                            $
                            // (plot_parent_id).find
								('.common_section.sec3').prepend('<div class="legendprint" >GPS Survey: Forest Rights Committee/Gramsabha <br/>Overlaying Data on Satellite Imagery &<br/>Generating Maps: ARCH, Vadodara<br/>Imagery Source: CARTOSAT 1 from NRSA, Hyderabad<br/>Imagery Date: December, 2005</div>')
							
        }
    }


    function load() {
        //total_loaded_plots = 0;
        stop_load = false;
        //alert(plotIds);
        if (plotIds == null) { ///ist time load
            if ($('#village :selected').text() == 'Select Village')
            {
                alert('Please select village');
                return;
            }
            $('#plotAppend').html('');
//            $.post(serviceUrl + 'ajax-get-plots-claim-village',
//            {vilid: $('#village').val(), claimant_ids: claimant_ids, claim_no: claim_no, claimstat: $('#claimstat').val()}, function (data) {
//                if (data.plotIdList) {
//                    plotIds = data.plotIdList.split(',');
//                    plotIdLen = plotIds.length - 1;
//                    //alert(plotIdLen);
//                    $('#loadRec').children(':first').val(plotIds.length);
//                    initiatePlotLoading();
//                } else {
//                    alert('No plots found for this village.Try with another village.');
//                }
//            }, 'json');
            $.post(laravelBaseUrl + 'plot-id-for-report',
                    {village: $('#village').val(), 'moholla': $('#mohalla').val(), claimant: claimant_ids, claim: claim_no, claimstat: $('#claimstat').val(), '_token': CSRFLaravel}, function (data) {
                if (data.plotIdList) {
                    plotIds = data.plotIdList.split(',');
                    plotIdLen = plotIds.length - 1;
                    //alert(plotIdLen);
                    $('#loadRec').children(':first').val(plotIds.length);
                    initiatePlotLoading();
                } else {
                    alert('No plots found for this village.Try with another village.');
                }
            }, 'json');
        } else { // 2nd time onwards load
            initiatePlotLoading();
        }
    }
    function initiatePlotLoading() { //no. of plots to load,index of last loaded plot
        var no_plot_load = parseInt($('#loadRec').val());
        $('#loader').show();
        $('#reset').css('display', 'none');
        //$('#load-btn').css('cursor', 'not-allowed');
        $('.load_btn_box').hide();
        $('.stop_load_box').css('display', 'inline');
        $('.load-msg').css('display', 'inline');
        // var total_plots_to_be_loaded = $('#loadRec option:selected').val();
        $('.total-plots').text(plotIds.length);
        createPlotHtmlRecursive(last_loded_plot_index, last_loded_plot_index + no_plot_load);
    }

    function createPlotHtmlRecursive(currentIndex, loadUpTo) {
        if (typeof (plotIds[currentIndex]) === 'undefined') {
            alert('No more plot to load!!C__xampp__htdocs__RTP__application__views__report__view_plot_scripts-php');
        }
        if (currentIndex < loadUpTo && stop_load == false && typeof (plotIds[currentIndex]) !== 'undefined') {

            $.ajax({
                url: serviceUrl + 'ajax-get-plotlayout/' + plotIds[currentIndex] + '/true',
                type: 'POST',
                data: {
                    vilName: $('#village :selected').text(),
                    state: $('#state :selected').text(),
                    district: $('#district :selected').text(),
                    block: $('#block :selected').text()
                },
                error: function () {
                    // $('#info').html('<p>An error has occurred</p>');
                },
                dataType: 'json',
                asyn: false,
                success: function (data) {

                    var plot_id = data.plotID;

                    var parent_id = "plot" + plot_id;
                    var plot_parent_id = "#" + parent_id;

                    var sat_value = data.pltDtl.satvalue;
                    var coordinates = '';
                    if (sat_value == 't') {
                        coordinates = data.coordinates.sat;
                        plotDetail[plot_id] = {"sat": true}
                    } else {
                        coordinates = data.coordinates.gps;
                        plotDetail[plot_id] = {"sat": false}
                    }
                    //console.log([);

                    //alert(typeof coordinates);
                    var coorodinates_arr = JSON.parse(coordinates);
                    var start_lat = '';
                    var start_lon = '';
                    if (coorodinates_arr != '') {
                        var start_point = coorodinates_arr[0].split(',');
                        start_lat = start_point[0];
                        start_lon = start_point[1];
                    }

                    // console.log(coorodinates_arr);
                    plot_coordinate_section = getCoordinateHtml(coorodinates_arr, plot_id);
                    //plot_coordinate_section = plot_coordinate_section.replace("/", "");
                    var html = '<div id="' + parent_id + '" class="plotencloser" >' + plot_layout_html + plot_coordinate_section +
                            '</div>' +
                            '</div>' +
                            '</div>' +
                            '</div>' +
                            '</div></div>';
                    $('#plotAppend').append(html);

                    //
            
					
                    //

                    var old_plot_id = data.old_plot_id;
                    var old_plot_url = 'http://www.righttoproperty.org/plot/view/id/' + old_plot_id;
                    var kml_url = 'http://127.0.0.1:8080/righttoproperty/kml/plot/' + plot_id + '/true';
                    $(plot_parent_id).find('.view_old_url').attr('href', old_plot_url)
                    $(plot_parent_id).find('.plot_kml_download').attr('href', kml_url)
                    $(plot_parent_id).find('.reload_plot').attr('onclick', 'reloadSinglePlotHtml(' + currentIndex + ')');
                    $(plot_parent_id).find('.state').text(data.stateName);
                    $(plot_parent_id).find('.tehsil').text(data.blockName);
                    $(plot_parent_id).find('.plot-no').text(data.pltDtl.plt_number);
                    $(plot_parent_id).find('.claim-no').text(data.pltDtl.claimnum);
                    $(plot_parent_id).find('.claiment-id').text(data.pltDtl.claimantoldid);
                    $(plot_parent_id).find('.claiment-name').text(data.pltDtl.claimantname);
                    $(plot_parent_id).find('.district').text(data.distName);
                    $(plot_parent_id).find('.res-village').text(data.pltDtl.resivillagename);
                    $(plot_parent_id).find('.plot-village').text(data.pltDtl.plotvillagename);
                    $(plot_parent_id).find('.claim-village').text(data.villageName);
//                    $(plot_parent_id).find('.gps-area').text(data.pltDtl.areagpssurveyh);
                    if (data.calc_area_hect_gps != null) {
                        var hect_area_gps = parseFloat(data.calc_area_hect_gps);
                        // var hect_area_sat = data.calc_area_hect_sat;
                        $(plot_parent_id).find('.gps-area').append('<span class="calc_gps_area">  ' + (hect_area_gps.toFixed(3)) + ' hectare </span><span class="calc_gps_area_acre" style="display:none">  ' + (hect_area_gps * 2.4710538).toFixed(3) + ' acre </span>');
                    }
//                    $(plot_parent_id).find('.sat-area').text(data.pltDtl.areaSat);
                    if (data.calc_area_hect_sat != null) {
                        var hect_area_sat = parseFloat(data.calc_area_hect_sat);
                        $(plot_parent_id).find('.sat-area').append('<span class="calc_sat_area">  ' + (hect_area_sat.toFixed(3)) + ' hectare </span><span class="calc_sat_area_acre" style="display:none">  ' + (hect_area_sat * 2.4710538).toFixed(3) + ' acre </span>');
                    }
                    $(plot_parent_id).find('.spouse-name').text(data.pltDtl.familymemname);
                    $(plot_parent_id).find('.map').attr('id', 'map' + plot_id)
                    if (sat_value == 't') {
                        $(plot_parent_id).find('.sat_or_gps_text').text('(SAT)');
                    } else {
                        $(plot_parent_id).find('.sat_or_gps_text').text('(GPS)');
                    }
                    loadmap(plot_id, start_lat, start_lon, coorodinates_arr);
                    $('.loaded-plots').text(++total_loaded_plots);
                    last_loded_plot_index++;
                    createPlotHtmlRecursive(currentIndex + 1, loadUpTo);
                    //total_loaded_plots++;
                },
            });
        } else {
            $('.area_container').css('visibility', 'visible');
            $('.coord_chage_container').css('visibility', 'visible');
            $('.change_all_to_decimal').css('display', 'inline');
            $('.print_btn').css('display', 'inline');
            $('#reset').css('display', 'inline');
            $('#loader').hide();
            $('.load_btn_box').css('display', 'inline');
            $('.stop_load_box').css('display', 'none');
            return false;
        }
    }

    function reloadSinglePlotHtml(currentIndex) {
        $.ajax({
            url: serviceUrl + 'ajax-get-plotlayout/' + plotIds[currentIndex] + '/true',
            type: 'POST',
            data: {
                vilName: $('#village :selected').text(),
                state: $('#state :selected').text(),
                district: $('#district :selected').text(),
                block: $('#block :selected').text()
            },
            error: function () {
                // $('#info').html('<p>An error has occurred</p>');
            },
            dataType: 'json',
            asyn: false,
            success: function (data) {

                var plot_id = data.plotID;
                var parent_id = "plot" + plot_id;
                var plot_parent_id = "#" + parent_id;

                var sat_value = data.pltDtl.satvalue;
                var coordinates = '';
                if (sat_value == 't') {
                    coordinates = data.coordinates.sat;
                } else {
                    coordinates = data.coordinates.gps;
                }
                //console.log([);

                //alert(typeof coordinates);
                var coorodinates_arr = JSON.parse(coordinates);
                var start_lat = '';
                var start_lon = '';
                if (coorodinates_arr != '') {
                    var start_point = coorodinates_arr[0].split(',');
                    start_lat = start_point[0];
                    start_lon = start_point[1];
                }

                // console.log(coorodinates_arr);
                plot_coordinate_section = getCoordinateHtml(coorodinates_arr, plot_id);
                //plot_coordinate_section = plot_coordinate_section.replace("/", "");
                var html = '<div id="' + parent_id + '">' + plot_layout_html + plot_coordinate_section +
                        //'</div> test-faiz' +
                        '</div>' +
                        '</div>' +
                        '</div>' +
                        '</div>' +
                        '</div></div>';
                $(plot_parent_id).html(html);
                var old_plot_id = data.old_plot_id;
                var old_plot_url = 'http://www.righttoproperty.org/plot/view/id/' + old_plot_id;
                var kml_url = 'http://127.0.0.1:8080/righttoproperty/kml/plot/' + plot_id + '/true';
                $(plot_parent_id).find('.view_old_url').attr('href', old_plot_url)
                $(plot_parent_id).find('.reload_plot').attr('onclick', 'reloadSinglePlotHtml(' + currentIndex + ')');
                $(plot_parent_id).find('.state').text(data.stateName);
                $(plot_parent_id).find('.tehsil').text(data.blockName);
                $(plot_parent_id).find('.plot-no').text(data.pltDtl.plt_number);
                $(plot_parent_id).find('.claim-no').text(data.pltDtl.claimnum);
                $(plot_parent_id).find('.claiment-id').text(data.pltDtl.claimantoldid);
                $(plot_parent_id).find('.claiment-name').text(data.pltDtl.claimantname);
                $(plot_parent_id).find('.district').text(data.distName);
                $(plot_parent_id).find('.res-village').text(data.pltDtl.resivillagename);
                $(plot_parent_id).find('.plot-village').text(data.pltDtl.plotvillagename);
                $(plot_parent_id).find('.claim-village').text(data.villageName);
//                $(plot_parent_id).find('.gps-area').text(data.pltDtl.areagpssurveyh);
                if (data.calc_area_hect_gps != null) {
                    var hect_area_gps = parseFloat(data.calc_area_hect_gps);
                    // var hect_area_sat = data.calc_area_hect_sat;
                    $(plot_parent_id).find('.gps-area').append('<span class="calc_gps_area">  ' + (hect_area_gps.toFixed(3)) + ' hectare </span><span class="calc_gps_area_acre" style="display:none">  ' + (hect_area_gps * 2.4710538).toFixed(3) + ' acre </span>');
                }
//                $(plot_parent_id).find('.sat-area').text(data.pltDtl.areaSat);
                if (data.calc_area_hect_sat != null) {
                    var hect_area_sat = parseFloat(data.calc_area_hect_sat);
                    $(plot_parent_id).find('.sat-area').append('<span class="calc_sat_area">  ' + (hect_area_sat.toFixed(3)) + ' hectare </span><span class="calc_sat_area_acre" style="display:none">  ' + (hect_area_sat * 2.4710538).toFixed(3) + ' acre </span>');
                }
                $(plot_parent_id).find('.spouse-name').text(data.pltDtl.familymemname);
                $(plot_parent_id).find('.map').attr('id', 'map' + plot_id)
                if (sat_value == 't') {
                    $(plot_parent_id).find('.sat_or_gps_text').text('(SAT)');
                } else {
                    $(plot_parent_id).find('.sat_or_gps_text').text('(GPS)');
                }
                loadmap(plot_id, start_lat, start_lon, coorodinates_arr);

            },
        });
    }

    function loadmap(plot_id, lat, lon, coorodinates_arr) {
        var pointVectorSource = new ol.source.Vector();
        window['map' + plot_id] =
                new ol.Map({
                    layers: [new ol.layer.Tile({
                            visible: bingvisible,
//                        preload: Infinity,
                            source: new ol.source.BingMaps({
                                key: 'Aq8wYwFzJ6vc0gaqMNZAHC42yBa1629gN5ybhNp0A-Ojzwvw4aAJUdhJHfcUfx9Y',
                                imagerySet: 'AerialWithLabels',
                                maxZoom: 19
                                        // use maxZoom 19 to see stretched tiles instead of the BingMaps
                                        // "no photos at this zoom level" tiles
                                        // maxZoom: 19
                            }),
                            zIndex: 1
                        })],
                    target: 'map' + plot_id,
//                    renderer: 'webgl',
                    view: new ol.View({
                        projection: 'EPSG:3857',
//                      center: [73.80845276, 21.67167644],
                        center: [0, 0],
                        zoom: 0,
//                                    maxResolution: 0.703125
                    }),
                    controls: ol.control.defaults().extend([
                        new ol.control.ScaleLine()
                    ])
                });
//        console.log(lat);//73.6907795
//        console.log(typeof lat)
//        console.log(lon);//21.6632419
//        var thing = new ol.geom.Point(ol.proj.transform([parseFloat(lat), parseFloat(lon)], 'EPSG:4326', 'EPSG:3857'));
//        var featurething = new ol.Feature({
//            name: "StartPoint",
//            geometry: thing
//        });
//        pointVectorSource.addFeature(featurething);

        for (i = 0, len = coorodinates_arr.length, text = ""; i < len; i++) {
            //console.log(coorodinates_arr[i]);

            var point = coorodinates_arr[i].split(',');
            point_lat = point[0];
            point_lon = point[1];
            var thing = new ol.geom.Point(ol.proj.transform([parseFloat(point_lat), parseFloat(point_lon)], 'EPSG:4326', 'EPSG:3857'));
            var featurething = new ol.Feature({
                name: "point" + i,
                geometry: thing
            });
            pointVectorSource.addFeature(featurething);
        }
        var gpsurl = '<?php echo base_url() ?>plotgpsGeoJSON/' + plot_id;// need to eliminate server side junk code
		
        var saturl = '<?php echo base_url() ?>plotsatGeoJSON/' + plot_id;
		//faiz test 
		$(document).ready(function(){
    $("button").click(function(){
        $("div").text("Hello world!");
    });
});



		
        window['plotSourceGPS' + plot_id] = new ol.source.Vector({
            url: gpsurl,
            format: new ol.format.GeoJSON({
                defaultDataProjection: 'EPSG:4326',
                projection: 'EPSG:3857'
            }),
        });
        window['plotSourceSAT' + plot_id] = new ol.source.Vector({
            url: saturl,
            format: new ol.format.GeoJSON({
                defaultDataProjection: 'EPSG:4326',
                projection: 'EPSG:3857'
            }),
            zIndex: 4
        });

        window['plotSourceGPS' + plot_id].on('change', function (event) {
            window['map' + plot_id].getView().fit(window['plotSourceGPS' + plot_id].getExtent(), window['map' + plot_id].getSize())
        });
        window['plotGPS' + plot_id] = new ol.layer.Vector({
            name: 'PlotGPS',
            style: createPolygonStyleFunction,
            zIndex: 3
        });
        window['plotStartPoint' + plot_id] = new ol.layer.Vector({
            name: 'PlotStartPoint',
            source: pointVectorSource,
            style: createPointStyleFunction,
            zIndex: 3
        });
        window['plotSAT' + plot_id] = new ol.layer.Vector({
            name: 'PlotSAT',
            style: createPolygonStyleFunctionSat,
            zIndex: 3
        });
        window['plotGPS' + plot_id].setSource(window['plotSourceGPS' + plot_id]);
        window['plotSAT' + plot_id].setSource(window['plotSourceSAT' + plot_id]);
        window['map' + plot_id].addLayer(window['plotGPS' + plot_id]);
        window['map' + plot_id].addLayer(window['plotSAT' + plot_id]);
        window['map' + plot_id].addLayer(window['plotStartPoint' + plot_id]);
//        var lonLat = new OpenLayers.LonLat( lon ,lat )
//          .transform(
//            new OpenLayers.Projection("EPSG:4326"), // transform from WGS 1984
//            window['map' + plot_id].getProjectionObject() // to Spherical Mercator Projection
//          );
//  
//    var markers = new OpenLayers.Layer.Markers( "Markers" );
//    window['map' + plot_id].addLayer(markers);
//    
//    markers.addMarker(new OpenLayers.Marker(lonLat));
    }
	
    var WMSlayer = [];
    function addWMSlayer(plotId) {
//		alert();

//        console.log(plotId);
//        console.log(ol.proj.transformExtent(map[plotId].getView().calculateExtent(map[plotId].getSize()),'EPSG:3857','EPSG:4326'));
        window['map' + plotId].getView().setZoom(17.125)
//            WMSlayer[plotId] = new ol.layer.Image({
            WMSlayer[plotId] = new ol.layer.Image({
//                        extent: window['plotSourceGPS' + plot_id].getExtent(),
                extent: window['map' + plotId].getView().calculateExtent(window['map' + plotId].getSize()), // window['plotSourceGPS' + plot_id].getExtent(),
//                    extent: ol.proj.transformExtent(map[plotId].getView().calculateExtent(map[plotId].getSize()), 'EPSG:3857', 'EPSG:4326'), // window['plotSourceGPS' + plot_id].getExtent(),
                source: new ol.source.ImageWMS({
//                        url: encodeURI('http://localhost/cgi-bin/mapserv.exe?map=D:\\mapserver\\actualimage.map'),
//                    url: encodeURI('http://localhost/cgi-bin/mapserv.exe?map=D:\\mapserver\\testmap.map'),
                    url: encodeURI('http://righttoproperty.org:8081/cgi-bin/mapserv.exe?map=C:\\ms4w\\HistoricalImage\\testmap.map'),
                    params: {
                        'LAYERS': 'Gujarat',
                    },
                    serverType: 'mapserver',
                    //                        projection: ol.proj.get('EPSG:32643'),
                }),
                zIndex: 2,
                brightness: 90,
                opacity: .5
            })
            window['map' + plotId].addLayer(WMSlayer[plotId]);

        //on wms load trigger next plot
    }

    function convertLatLongDECtoDMS(value) {
        var vars = value.split('.');
        //console.log(vars);
        var deg = vars[0];
        var tempma = "0." + vars[1];

        tempma = tempma * 3600;
        var min = Math.floor(tempma / 60);
        var sec = tempma - (min * 60);

        return deg + '&deg;' + min + '\'' + sec.toFixed(4) + '"';
    }



    function getCoordinateHtml(coordinates, plot_id) {

        //var coords_length = 45;
        var header = '<div class="common_section sec3">' +
                '<h2>Set Of Coordinates <span class="sat_or_gps_text"><span></h2>' +
                //'<a class="change_to_decimal" href="javascript:void(0)">Change to Decimal</a>' +
                //'<a class="change_to_degree" href="javascript:void(0)" style="display:none">Change to Degree</a>' +
                '<div class="common_content1 cord_table_pagebrk">' +
                '<div class="row">' +
                '<div class="col-sm-4 col-xs-4 col-md-4 col-lg-4 box_adjust">' +
                '<h4 class="plot_heading_prnt">Plot no : <span class="plot-no"></span> ,Claim no : <span class="claim-no"></span>,Claimant Id : <span class="claiment-id"></span> <span class="custom-page-num"></span></h4>' +
                '<div class="cord_table cord_table_degree"><table>' +
                '<tr>' +
                //'<th>No.__Faiz</th>' +
                '<th>No.</th>' +
                '<th>Latitude</th>' +
                '<th>Longitude</th>' +
                '<th>No.</th>' +
                '<th>Latitude</th>' +
                '<th>Longitude</th>' +
                '<th>No.</th>' +
                '<th>Latitude</th>' +
                '<th>Longitude</th>' +
                '</tr>';
        var td_html = '';
        $.each(coordinates, function (index, value) {
            var coords = value.split(',');
            var lat = coords[1];
            var lon = coords[0];
            //(plot_id, lat, lon);
            if ((index % 3) == 0)
                td_html += '<tr>';
            if ((index % 3) == 0) {
                td_html += '<td>' + (index + 1) + '</td>';
                td_html += '<td>' + convertLatLongDECtoDMS(lat) + '</td>';
                td_html += '<td>' + convertLatLongDECtoDMS(lon) + '</td>';
            }
            if ((index % 3) == 1) {
                td_html += '<td>' + (index + 1) + '</td>';
                td_html += '<td>' + convertLatLongDECtoDMS(lat) + '</td>';
                td_html += '<td>' + convertLatLongDECtoDMS(lon) + '</td>';
            }
            if ((index % 3) == 2) {
                td_html += '<td>' + (index + 1) + '</td>';
                td_html += '<td>' + convertLatLongDECtoDMS(lat) + '</td>';
                td_html += '<td>' + convertLatLongDECtoDMS(lon) + '</td>';
            }
            if ((index % 3) == 2)
                td_html += '</tr>';
        });
        header += td_html;
        header += '</table>' +
                '</div>' +
                '<div class="cord_table cord_table_decimal" style="display:none"><table>' +
                '<tr>' +
                //'<th>No.__Faiz</th>' +
                '<th>No.</th>' +
                '<th>Latitude</th>' +
                '<th>Longitude</th>' +
                '<th>No.</th>' +
                '<th>Latitude</th>' +
                '<th>Longitude</th>' +
                '<th>No.</th>' +
                '<th>Latitude</th>' +
                '<th>Longitude</th>' +
                '</tr>';
        var td_html = '';
        $.each(coordinates, function (index, value) {
            var coords = value.split(',');
            var lat = coords[1];
            var lon = coords[0];
            //highlightCoordinate(plot_id, lat, lon);
            if ((index % 3) == 0)
                td_html += '<tr>';
            if ((index % 3) == 0) {
                td_html += '<td>' + (index + 1) + '</td>';
                td_html += '<td>' + (lat) + '</td>';
                td_html += '<td>' + (lon) + '</td>';
            }
            if ((index % 3) == 1) {
                td_html += '<td>' + (index + 1) + '</td>';
                td_html += '<td>' + (lat) + '</td>';
                td_html += '<td>' + (lon) + '</td>';
            }
            if ((index % 3) == 2) {
                td_html += '<td>' + (index + 1) + '</td>';
                td_html += '<td>' + (lat) + '</td>';
                td_html += '<td>' + (lon) + '</td>';
            }
            if ((index % 3) == 2)
                td_html += '</tr>';
        });
        header += td_html;

        header += '</table>' +
                '</div>' +
                '</div>' +
                '</div>' +
                '</div> ' +
                '</div>';

        return header;

    }

    function resetForm() {
        bingvisible = false;
        $('#satimgchkbx').prop('checked', false);
        $('#regionNav')[0].reset();
        plotIds = null;
        $('#plotAppend').html('');
        //console.log(plotIds);
        last_loded_plot_index = 0;
        $(".chosen-select").chosen("destroy");
        $('#claimant').children(':not(:first-child)').remove();
        total_loaded_plots = 0;
        $('.load-msg').hide();
        $('#print').css('display', 'none');
        $('.change_all_to_decimal').css('display', 'none');
        $('.change_all_to_degree').css('display', 'none');
        $('.area_container').css('visibility', 'hidden');
        $('.coord_chage_container').css('visibility', 'hidden');
    }
    $(document).ready(function () {


        $.get(laravelCSRFUrl, function (data) {
            CSRFLaravel = data
        });
//        $("#claimant").chosen().change(function () {
//            var claimant_id = $(this).val();
//            //alert(id);
//            var village_id = $('#village :selected').val();
//            $("#claim").chosen("destroy");
//            populateClaims(village_id, claimant_id);
//        });

        $('body').delegate('.change_all_to_decimal', 'click', function () {
            $('.change_all_to_degree').css('display', 'inline');
            $(this).css('display', 'none');

            $('.cord_table_decimal').css('display', 'block');
            $('.cord_table_degree').css('display', 'none');
        });

        $('body').delegate('.change_all_to_degree', 'click', function () {
            //alert('sdcs');
            $('.change_all_to_decimal').css('display', 'inline');
            $(this).css('display', 'none');

            $('.cord_table_decimal').css('display', 'none');
            $('.cord_table_degree').css('display', 'block');
        });

//        $('body').delegate('.change_to_decimal', 'click', function () {
//            $(this).parents('.common_section').find('.change_to_degree').css('display', 'block');
//            $(this).css('display', 'none');
//
//            $(this).parents('.common_section').find('.cord_table_decimal').css('display', 'block');
//            $(this).parents('.common_section').find('.cord_table_degree').css('display', 'none');
//        });
//
//        $('body').delegate('.change_to_degree', 'click', function () {
//            $(this).parents('.common_section').find('.change_to_decimal').css('display', 'block');
//            $(this).css('display', 'none');
//
//            $(this).parents('.common_section').find('.cord_table_decimal').css('display', 'none');
//            $(this).parents('.common_section').find('.cord_table_degree').css('display', 'block');
//        });

        $('#claim,#claimant').on('change', function () {

            last_loded_plot_index = 0;
            claim_no = $('#claim').val();
            claimant_ids = $('#claimant').val();
            $('#plotAppend').html('');
            // claimant_ids = $('#claimant').val();
            // alert($(this).val());
            if (plotIds != null) {
                total_loaded_plots = 0;
                // $.post(serviceUrl + 'ajax-get-plots-claim-village', {vilid: $('#village').val(), claimant_ids: claimant_ids, claim_no: claim_no}, function (data) {
                $.post(laravelBaseUrl + 'plot-id-for-report',
                        {village: $('#village').val(), 'moholla': $('#mohalla').val(), claimant: claimant_ids, claim: claim_no, claimstat: $('#claimstat').val(), '_token': CSRFLaravel}, function (data) {
                    if (data.plotIdList) {
                        plotIds = data.plotIdList.split(',');
                        plotIdLen = plotIds.length - 1;
                        $('#loadRec').children(':first').val(plotIds.length);
                    }
                }, 'json');
            }
        });
//        $('#claimant').on('change', function () {
//            claimant_ids = $(this).val();
//            // alert($(this).val());
//        });

        $.get(serviceUrl + 'ajax-states', function (data) {
            $.each(data, function (index, value) {
                $('#state').append('<option oncontextmenu="alert($(this).val());" value="' + $(this)[0].Id + '" >' + $(this)[0].Name + '</option>')
            });
        }, 'json');
    })
</script>
<!-- togglt panel ends  -->