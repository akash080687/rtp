<div class="row" id="plot<?php echo $plotID ?>">
    <div class="col-sm-12">
        <div class="plot_map">
            <h1>Corrected Plot Reports -</h1>
            <div class="gray_border">
                <div class="common_section">
                    <h2>GPS Plot Detail - </h2>
                    <div class="common_content1">
                        <div class="row">
                            <div class="col-sm-6 col-xs-6 col-md-6 col-lg-6">
                                <div class="table_container">
                                    <table>
                                        <tr>
                                            <td>State</td>
                                            <td>:</td>
                                            <td><?php echo $stateName ?></td>
                                        </tr>
                                        <tr>
                                            <td>Tehsil</td>
                                            <td>:</td>
                                            <td><?php echo $blockName ?></td>
                                        </tr>
                                        <tr>
                                            <td>Plot No.</td>
                                            <td>:</td>
                                            <td><?php echo $pltDtl['plt_number']; ?></td>
                                        </tr>
                                        <tr>
                                            <td>Claim No.</td>
                                            <td>:</td>
                                            <td><?php echo $pltDtl['claimnum']; ?></td>
                                        </tr>
                                        <tr>
                                            <td>Claiment ID</td>
                                            <td>:</td>
                                            <td><?php echo $pltDtl['claimantoldid']; ?></td>
                                        </tr>
                                        <tr>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td>Claiment Name</td>
                                            <td>:</td>
                                            <td><?php echo $pltDtl['claimantname']; ?></td>
                                        </tr>
                                    </table>
                                </div>
                            </div>

                            <div class="col-sm-6 col-xs-6 col-md-6 col-lg-6">
                                <div class="table_container">
                                    <table>
                                        <tr>
                                            <td>District</td>
                                            <td>:</td>
                                            <td><?php echo $distName ?></td>
                                        </tr>
                                        <tr>
                                            <td>Resident Village</td>
                                            <td>:</td>
                                            <td><?php echo $pltDtl['resivillagename']; ?></td>
                                        </tr>
                                        <tr>
                                            <td>Plot Village</td>
                                            <td>:</td>
                                            <td><?php echo $pltDtl['plotvillagename']; ?></td>
                                        </tr>
                                        <tr>
                                            <td>Claim Village</td>
                                            <td>:</td>
                                            <td><?php echo $villageName ?></td>
                                        </tr>
                                        <tr>
                                            <td>GPS Area in Hectares</td>
                                            <td>:</td>
                                            <td><?php echo sprintf("%.2f",$pltDtl['areagpssurveyh']);//echo '  '.($pltDtl['area']*2.47105);  ?></td>
                                        </tr>
                                        <tr>
                                            <td>Satellite Area in Hectares</td>
                                            <td>:</td>
                                            <td><?php echo sprintf("%.2f",$pltDtl['areaSat']); ?></td>
                                        </tr>
                                        <tr>
                                            <td>Spouse Name</td>
                                            <td>:</td>
                                            <td><?php echo $pltDtl['familymemname']; ?></td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <!--  -->
                <div class="common_section--">
                    <!--                    <h2>
                                            <a class="icon_btn" href="#"><img src="images/expand.png" alt="" /></a>
                                            <a class="icon_btn" href="#"><img src="images/collapse.png" alt="" /></a>
                                            <span class="arc_btn"><a href="#">Show Archival Images</a></span>
                                        </h2>-->
                    <div class="common_content2">
					test
                        <div id="map<?php echo $plotID ?>" class="map" style="padding: 0 5px;"></div>
                        <script>
						
                            var map<?php echo $plotID ?> = new ol.Map({
                                target: 'map<?php echo $plotID ?>',
                                layers: [],
                                view: new ol.View({
                                    projection: 'EPSG:4326',
//                    center: [73.80845276, 21.67167644],
                                    center: [0, 0],
                                    zoom: 17,
                                    maxResolution: 0.703125
                                }),
                                controls: ol.control.defaults().extend([
                                    new ol.control.ScaleLine()
                                ])
                            });
                            var url = '<?php echo base_url() ?>plotgpsGeoJSON/<?php echo $plotID ?>';
                            var plotSourceGPS<?php echo $plotID ?> = new ol.source.Vector({
                                url: url,
                                format: new ol.format.GeoJSON({
                                    defaultDataProjection: 'EPSG:4326',
                                    projection: 'EPSG:3857'
                                }),
//                                loader: map.getView().fit(plotSourceGPS.getExtent(), map.getSize())
                            });
                            var url = '<?php echo base_url() ?>plotsatGeoJSON/<?php echo $plotID ?>';
                            var plotSourceSAT<?php echo $plotID ?> = new ol.source.Vector({
                                url: url,
                                format: new ol.format.GeoJSON({
                                    defaultDataProjection: 'EPSG:4326',
                                    projection: 'EPSG:3857'
                                }),
//                                loader: map.getView().fit(plotSourceGPS.getExtent(), map.getSize())
                            });
                            plotSourceGPS<?php echo $plotID ?>.on('tileloadend', function (event) {
                                map<?php echo $plotID ?>.getView().fit(plotSourceGPS<?php echo $plotID ?>.getExtent(), map<?php echo $plotID ?>.getSize())
                            });
//                            plotSourceSAT<?php echo $plotID ?>.on('tileloadend', function (event) {
//                                map<?php echo $plotID ?>.getView().fit(plotSourceSAT<?php echo $plotID ?>.getExtent(), map<?php echo $plotID ?>.getSize())
//                            });
                            var plotGPS<?php echo $plotID ?> = new ol.layer.Vector({
                                name: 'PlotGPS',
                                style: createPolygonStyleFunction
                            });
                            var plotSAT<?php echo $plotID ?> = new ol.layer.Vector({
                                name: 'PlotSAT',
                                style: createPolygonStyleFunctionSat
                            });
                            plotGPS<?php echo $plotID ?>.setSource(plotSourceGPS<?php echo $plotID ?>);
                            plotSAT<?php echo $plotID ?>.setSource(plotSourceSAT<?php echo $plotID ?>);
                            map<?php echo $plotID ?>.addLayer(plotGPS<?php echo $plotID ?>);
                            map<?php echo $plotID ?>.addLayer(plotSAT<?php echo $plotID ?>);
                            setTimeout(function () {
                                map<?php echo $plotID ?>.getView().fit(plotSourceGPS<?php echo $plotID ?>.getExtent(), map<?php echo $plotID ?>.getSize())
                            }, 1000);
                            setTimeout(function () {
                                map<?php echo $plotID ?>.getView().fit(plotSourceSAT<?php echo $plotID ?>.getExtent(), map<?php echo $plotID ?>.getSize())
                            }, 1000);
                             setTimeout(function () {
                                map<?php echo $plotID ?>.getView().fit(plotSourceGPS<?php echo $plotID ?>.getExtent(), map<?php echo $plotID ?>.getSize())
                            }, 1000);
							
                        </script>
                    </div>
                </div>
                <!--  -->
                <!--  -->
                <div class="common_section">
                    <h2>Set Of Coordinates - <?php echo '('.$coord_type.')';  ?></h2>
                    <div class="common_content1">
                        <div class="row">
                            <div class="col-sm-4 col-xs-4 col-md-4 col-lg-4 box_adjust">
                                <div class="cord_table">
                                    <table>
                                        <tr>
                                            <th>No.</th>
                                            <th>Latitude</th>
                                            <th>Longitude</th>
                                            <th>No.</th>
                                            <th>Latitude</th>
                                            <th>Longitude</th>
                                            <th>No.</th>
                                            <th>Latitude</th>
                                            <th>Longitude</th>
                                        </tr>
                                        <?php if(!empty($coordinates))foreach ($coordinates as $key => $value) : ?>
                                            <?php if ($key % 3 == 0): ?> <tr> <?php endif; ?>
                                                <?php if ($key % 3 == 0): ?>
                                                    <td><?= $key+1 ?></td>
                                                    <td><?php  $DMSValue = $this->all_function->DECtoDMS($value[0]);echo $DMSValue['deg'].'&deg;'.$DMSValue['min'].'\''.round($DMSValue['sec'], 4).'"'; ?></td>
                                                    <td><?php  $DMSValue = $this->all_function->DECtoDMS($value[1]);echo $DMSValue['deg'].'&deg;'.$DMSValue['min'].'\''.round($DMSValue['sec'], 4).'"'; ?></td>
                                                <?php endif; ?>
                                                <?php if ($key % 3 == 1): ?>
                                                    <td><?= $key+1 ?></td>
                                                    <td><?php  $DMSValue = $this->all_function->DECtoDMS($value[0]);echo $DMSValue['deg'].'&deg;'.$DMSValue['min'].'\''.round($DMSValue['sec'], 4).'"'; ?></td>
                                                    <td><?php  $DMSValue = $this->all_function->DECtoDMS($value[1]);echo $DMSValue['deg'].'&deg;'.$DMSValue['min'].'\''.round($DMSValue['sec'], 4).'"'; ?></td>
                                                <?php endif; ?>
                                                <?php if ($key % 3 == 2): ?>
                                                    <td><?= $key+1 ?></td>
                                                    <td><?php  $DMSValue = $this->all_function->DECtoDMS($value[0]);echo $DMSValue['deg'].'&deg;'.$DMSValue['min'].'\''.round($DMSValue['sec'], 4).'"'; ?></td>
                                                    <td><?php  $DMSValue = $this->all_function->DECtoDMS($value[1]);echo $DMSValue['deg'].'&deg;'.$DMSValue['min'].'\''.round($DMSValue['sec'], 4).'"'; ?></td>
                                                <?php endif; ?>
                                                <?php if ($key % 3 == 2): ?></tr><?php endif; ?>
                                        <?php endforeach; ?>
                                    </table>
                                </div>
                            </div>


                            <!--

                            <div class="col-sm-4 col-xs-4 col-md-4 col-lg-4 box_adjust">
                                <div class="cord_table">
                                    <table>
                                        <tr>
                                            <th>No.</th>
                                            <th>Latitude</th>
                                            <th>Longitude</th>
                                        </tr>

                                        <tr>
                                            <td>1</td>
                                            <td>21.39' 46.731"</td>
                                            <td>21.39' 46.731"</td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                            <div class="col-sm-4 col-xs-4 col-md-4 col-lg-4 box_adjust">
                                <div class="cord_table">
                                    <table>
                                        <tr>
                                            <th>No.</th>
                                            <th>Latitude</th>
                                            <th>Longitude</th>
                                        </tr>

                                        <tr>
                                            <td>1</td>
                                            <td>21.39' 46.731"</td>
                                            <td>21.39' 46.731"</td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                            -->

                            <!--
                          <div class="common_content1">
                              <div class="row">
                                  <div class="col-sm-4 col-xs-4 col-md-4 col-lg-4 box_adjust">
                                      <div class="cord_table">
                                          <table>

                                              <tr>
                                                  <td>1</td>
                                                  <td>21.39' 46.731"</td>
                                                  <td>21.39' 46.731"</td>
                                              </tr>
                                          </table>
                                      </div>
                                  </div>
                                  <div class="col-sm-4 col-xs-4 col-md-4 col-lg-4 box_adjust">
                                      <div class="cord_table">
                                          <table>

                                              <tr>
                                                  <td>1</td>
                                                  <td>21.39' 46.731"</td>
                                                  <td>21.39' 46.731"</td>
                                              </tr>
                                          </table>
                                      </div>
                                  </div>
                                  <div class="col-sm-4 col-xs-4 col-md-4 col-lg-4 box_adjust">
                                      <div class="cord_table">
                                          <table>

                                              <tr>
                                                  <td>1</td>
                                                  <td>21.39' 46.731"</td>
                                                  <td>21.39' 46.731"</td>
                                              </tr>
                                          </table>
                                      </div>
                                  </div>
                              </div>
                          </div>


                          <div class="common_content1">
                              <div class="row">
                                  <div class="col-sm-4 col-xs-4 col-md-4 col-lg-4 box_adjust">
                                      <div class="cord_table">
                                          <table>

                                              <tr>
                                                  <td>1</td>
                                                  <td>21.39' 46.731"</td>
                                                  <td>21.39' 46.731"</td>
                                              </tr>
                                          </table>
                                      </div>
                                  </div>
                                  <div class="col-sm-4 col-xs-4 col-md-4 col-lg-4 box_adjust">
                                      <div class="cord_table">
                                          <table>

                                              <tr>
                                                  <td>1</td>
                                                  <td>21.39' 46.731"</td>
                                                  <td>21.39' 46.731"</td>
                                              </tr>
                                          </table>
                                      </div>
                                  </div>
                                  <div class="col-sm-4 col-xs-4 col-md-4 col-lg-4 box_adjust">
                                      <div class="cord_table">
                                          <table>

                                              <tr>
                                                  <td>1</td>
                                                  <td>21.39' 46.731"</td>
                                                  <td>21.39' 46.731"</td>
                                              </tr>
                                          </table>
                                      </div>
                                  </div>
                              </div>
                          </div>


                          <div class="common_content1">
                              <div class="row">
                                  <div class="col-sm-4 col-xs-4 col-md-4 col-lg-4 box_adjust">
                                      <div class="cord_table">
                                          <table>

                                              <tr>
                                                  <td>1</td>
                                                  <td>21.39' 46.731"</td>
                                                  <td>21.39' 46.731"</td>
                                              </tr>
                                          </table>
                                      </div>
                                  </div>
                                  <div class="col-sm-4 col-xs-4 col-md-4 col-lg-4 box_adjust">
                                      <div class="cord_table">
                                          <table>

                                              <tr>
                                                  <td>1</td>
                                                  <td>21.39' 46.731"</td>
                                                  <td>21.39' 46.731"</td>
                                              </tr>
                                          </table>
                                      </div>
                                  </div>
                                  <div class="col-sm-4 col-xs-4 col-md-4 col-lg-4 box_adjust">
                                      <div class="cord_table">
                                          <table>

                                              <tr>
                                                  <td>1</td>
                                                  <td>21.39' 46.731"</td>
                                                  <td>21.39' 46.731"</td>
                                              </tr>
                                          </table>
                                      </div>
                                  </div>
                              </div>
                          </div>
                            -->


                            <!--  -->
                        </div>
                        <!--  -->
                    </div>
                    <!--  -->
                    <!----------------------------------------------------------->
                    <!--  -->
                    <!--  -->
                </div>
            </div>
        </div>
    </div>
</div>