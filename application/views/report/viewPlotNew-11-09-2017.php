<script src="js/custom/plot_layout.js"></script>
<link rel="stylesheet" href="http://openlayers.org/en/v3.16.0/css/ol.css" type="text/css">
<script src="http://openlayers.org/en/v3.16.0/build/ol.js"></script>
<script>
    var bingvisible = false;
</script>
<?php $this->load->view('report/view_plot_scripts') ?> 
<div class="container">      
    <div class="container print_hide">
        <div class="row">

            <div class="col-sm-12">
                <div>
                    <ol class="breadcrumb">
                        <li><a href="http://righttoproperty.org/">Home</a></li>                     
                        <li><a href="javascript:void(0);">Reports</a></li>
                        <li class="active">Village Plots</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
</div>
<style>
    /*    .googleMap {
            width: 100%;
            height: 100%;
        }*/
    .chosen_select_custom {
        background: #f0f0f0;
        border: 1px solid #ccc;
        border-radius: 3px;
        height: 30px !important;
        padding: 6px 0 2px;
        width: 250px;
    }

    .loadnum{
        border: 1px solid #00aa01;
        border-radius: 0 5px 5px 0;
        height: 48px;
        padding: 4px;
        width:70px;
        background:#00aa01;
        box-shadow: 0 2px 0 #007300;
        vertical-align: middle;
        color: #fff;
        font-size: 20px;
        text-shadow: 0 0 1px #000101;
    }

    .submit_btn_drop {
        display: inline-block;
        margin: 0 3px;
    }

    .submit_btn_drop a {
        vertical-align: middle;
        background: #00aa01;
        border-left: 1px solid #00aa01;
        border-right: 1px solid #034403;
        border-top: 1px solid #00aa01;
        border-bottom: 1px solid #00aa01;
        border-radius: 5px 0 0 5px;
        box-shadow: 0 2px 0 #007300;
        color: #fff;
        font-size: 20px;
        letter-spacing: 2px;
        padding: 11px 5px 11px 38px;
        text-decoration: none;
        text-shadow: 0 0 1px #000101;
        text-transform: uppercase;
        text-decoration: none;
        transition: all .5s ease-in-out;
    }
    .submit_btn_drop a:hover {
        background: #007800;
        border: 1px solid #007800;
        text-shadow: none;
        text-decoration: none;
        color: #fff;
    }

    @page {
        @bottom-right {
            content: counter(page) " of " counter(pages);
        }
    }


</style>
<!-- breadcrumb ends  -->

<!-- togglt bar starts  -->
<div class="toggle_bar_container">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 no_padding">
                <div class="toggle_bar">
                    <div class="toggle_bar_left">Search Plot</div>
                    <div class="toggle_bar_right pull-right">
                        <a ng-show="search" id="view_panel" ng-click="search = false" onclick="$('#view_panel').hide(600, function () {
                                    $('#hide_panel').show();
                                    $('#display_panel').show();
                                })" style="display: none;" ><img src="images/down.png" alt="" /></a>
                        <a ng-hide="search" id="hide_panel" ng-click="search = true" onclick="$('#hide_panel').hide(600, function () {
                                    $('#view_panel').show();
                                    $('#display_panel').hide();
                                })" ><img src="images/up.png" alt="" /></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<!-- togglt bar ends  -->

<!-- togglt panel starts  -->
<form id="regionNav" >
    <div class="display_panel_container" id="display_panel" >
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="display_panel_content">

                        <div class="dropdown_container1">
                            <div class="custom_dropdown">
                                <div class="normal_text">State<span>*</span></div>
                                <div class="select-style">
                                    <select id="state" onchange="populateDistrict($(this).val());"  > <!--ng-options="state.name for state in states | orderBy : 'name' "-->
                                        <option value="">Select State</option>
                                    </select>
                                </div>
                            </div>
                            <div class="custom_dropdown">
                                <div class="normal_text">District<span>*</span></div>
                                <div class="select-style">
                                    <select id="district" onchange="populateBlock($(this).val())"   > <!--ng-options="district.name for district in districts | orderBy : 'name' "-->
                                        <option value="">Select District</option>
                                    </select>
                                </div>
                            </div>
                            <div class="custom_dropdown">
                                <div class="normal_text">Block/Tehsil<span>*</span></div>
                                <div class="select-style">
                                    <select id="block" onchange="populateVillage($(this).val())">
                                        <option value="">Select Block/Tehsil</option>
                                    </select>
                                </div>
                            </div>
                            <div class="custom_dropdown">
                                <div class="normal_text">
                                    <!--<button type="button" class="btn btn-primary" data-toggle="modal" data-target=".bs-example-modal-sm2" onclick="$('#vilcode').val($('#village').val())">Get Village Code</button><button type="button" class="btn btn-primary" data-toggle="modal" data-target=".bs-example-modal-sm">Add Village</button>-->
                                    Village<span>*</span></div>
                                <div class="select-style">
                                    <select id="village" onchange="
                                            plotIds = null;
                                            claimant_ids = [];
                                            claim_no = [];
                                            $('#plotAppend').html('');
                                            last_loded_plot_index = 0;
                                            $('.chosen-select').chosen('destroy');
                                            $('#claimant').children(':not(:first-child)').remove();
                                            $('#claim').children(':not(:first-child)').remove();
                                            total_loaded_plots = 0;
                                            $('.load-msg').css('display', 'none');
                                            populateMohalla($(this).val());
                                            populateClaimant($(this).val(), $('#mohalla').val());
                                            populateClaims($(this).val(), $('#mohalla').val(), '');
                                            ">
                                        <option value="Select Village">Select Village</option>
                                    </select>
                                </div>
                            </div>
                            <div class="custom_dropdown">
                                <div class="normal_text">Mohalla</div>
                                <div class="select-style">
                                    <select id="mohalla" onchange="
                                            plotIds = null;
                                            claimant_ids = [];
                                            claim_no = [];
                                            $('#plotAppend').html('');
                                            last_loded_plot_index = 0;
                                            $('.chosen-select').chosen('destroy');
                                            $('#claimant').children(':not(:first-child)').remove();
                                            $('#claim').children(':not(:first-child)').remove();
                                            total_loaded_plots = 0;
                                            $('.load-msg').css('display', 'none');
                                            populateClaimantByMoholla($('#village').val(), $(this).val());
                                            populateClaims($('#village').val(), $(this).val(), '');">
                                        <option value="">Select Mohalla</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="dropdown_container2">
                            <div class="custom_dropdown">
                                <div class="normal_text">Claimant ID</span></div>
                                <div class=""> 
                                    <select name="claimant_id[]" onchange="
//                                            plotIds = null;
//                                            
//                                            $('#plotAppend').html('');
//                                            last_loded_plot_index = 0;
//                                            total_loaded_plots = 0;
//                                            $('.load-msg').css('display', 'none');
                                            claim_no = [];
                                            $('.claim-chosen-select').chosen('destroy');
                                            $('#claim').children(':not(:first-child)').remove();
                                            populateClaims($('#village').val(), $('#mohalla').val(), $(this).val())" id="claimant" class="chosen-select chosen_select_custom" multiple>
                                        <option value="">Select Claimant ID</option>
                                    </select>
                                </div>
                            </div>
                            <div class="custom_dropdown">
                                <div class="normal_text">Claim No.</div>
                                <div class="">
                                    <select id="claim" name="claim[]" class="chosen-select chosen_select_custom claim-chosen-select" multiple>
                                        <option value="">Select Claim No.</option>
                                    </select>
                                </div>
                            </div>
                            <style>
                                #claimstat {
                                    background: #f0f0f0 none repeat scroll 0 0;
                                    border: 1px solid #ccc;
                                    border-radius: 3px;
                                    color: #686868;
                                    font-size: 13px;
                                    height: 31px;
                                    padding: 2px;
                                }
                            </style>
                            <div class="custom_dropdown">
                                <div class="normal_text">Claim Status</div>
                                <div class="">
                                    <select id="claimstat" name="claimstat[]" class="" >
                                        <option value="">Select Claim Status.</option>
                                        <option value="A1">A1 - Approved & no dispute</option>
                                        <option value="A2">A2 - Approved but area disputed</option>
                                        <option value="P">P - Pending</option>
                                    </select>
                                </div>
                            </div>
                            <div class="custom_dropdown" >
                                <div class="row">
                                    <div class="col-sm-4 col-md-4 col-lg-4">
                                        <div><a href="#"  onclick="load_historical_image(0);" class="btn btn-info">Historical imagery</a></div>
                                    </div>
                                    <div class="col-sm-4 col-md-4 col-lg-4" style="white-space: nowrap;">
                                        <div class="normal_text">Satellite Imagery</div>
                                    </div> 
                                    <div class="col-sm-4 col-md-4 col-lg-4">
                                        <div class="change_area_value_container">
                                            <input type="checkbox" id="satimgchkbx" onchange="bingvisible = $(this).is(':checked')" />

                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="custom_dropdown area_container" style="visibility:hidden">
                                <div class="normal_text">Area</div>
                                <div class="change_area_value_container">
                                    <span class="change_area_value hectar_to_acre" onclick="$(this).css('display', 'none');
                                            $('.acre_to_hectar').css('display', 'inline');
                                            $('.calc_gps_area_acre').css('display', 'inline');
                                            $('.calc_gps_area').css('display', 'none');
                                            $('.calc_sat_area_acre').css('display', 'inline');
                                            $('.calc_sat_area').css('display', 'none');
                                          "><a  href="javascript:void(0);">Hectare to Acre</a>
                                    </span>
                                    <span class="change_area_value acre_to_hectar" style="display: none"
                                          onclick="$(this).css('display', 'none');
                                                  $('.hectar_to_acre').css('display', 'inline');
                                                  $('.calc_gps_area_acre').css('display', 'none');
                                                  $('.calc_gps_area').css('display', 'inline');
                                                  $('.calc_sat_area_acre').css('display', 'none');
                                                  $('.calc_sat_area').css('display', 'inline');
                                          "><a  href="javascript:void(0);">Acre to Hectare</a>
                                    </span>
                                </div>
                            </div>


                            <div class="custom_dropdown coord_chage_container" style="visibility:hidden">
                                <div class="normal_text">Coordinates</div>
                                <div class="change_area_value_container">
                                    <span class="change_area_value change_all_to_decimal" style="display: none"><a class="" href="javascript:void(0);">Deg.Min.Sec to Decimal</a></span>
                                    <span class="change_area_value change_all_to_degree" style="display: none"><a  href="javascript:void(0);">Decimal to Deg.Min.Sec</a></span>
                                </div>
                            </div>

                        </div>

                        <div class="button_group">
                            <div class="load-msg" style="display: none">Loaded: <span class="loaded-plots"></span> of <span class="total-plots"></span></div>
                            <!--                            <div class="submit_btn"><a href="javascript:populatePlot();">submit</a></div> -->
                            <div class="submit_btn_drop load_btn_box"  ><a id="load-btn" onclick="load();" >Load</a><select class="loadnum" id="loadRec"><option value="0">All</option><option value="1" >1</option><option value="10" selected="true">10</option><option value="20">20</option></select></div> 
                            <!--                            <div class="submit_btn_drop"  ><a onclick="typeof (plotIds) == 'undefined' ? alert('Please Submit') : loadUntil += parseInt($('#loadRec').val());
                                                                loadMapRecursively(loadcount);" >Load</a><select class="loadnum" id="loadRec"><option value="0">All</option><option value="1" >1</option><option value="10" >10</option><option value="20">20</option></select></div> -->
                            <div class="reset_btn stop_load_box" style="display: none;"><a  href="javascript:void(0);" onclick="stop_load = true;">stop loading</a></div>
                            <div class="reset_btn" id="reset"><a  href="javascript:void(0);" onclick="resetForm();" >reset</a></div>
                            <div class="print_btn" id="print" style="display: none"><a  href="javascript:void(0);" onclick="window.print()" >print</a></div>
                            <!--                            <div class="reset_btn change_all_to_decimal" style="display: none"><a class="" href="javascript:void(0);">Deg.Min.Sec to Decimal</a></div>
                                                        <div class="reset_btn change_all_to_degree" style="display: none"><a  href="javascript:void(0);">Decimal to Deg.Min.Sec</a></div>-->


                        </div>

                        <div id="loader" class="loader" style="display: none;"><img src="<?= base_url() ?>images/loader.gif" /></div>


                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
</form>

<!-- togglt panel ends  -->

<div class="plot_map_container">
    <div class="container" id="plotAppend">

    </div>
</div>

<div id="content">
    <div id="pageFooter"></div>
</div>