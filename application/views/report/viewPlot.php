<script src="js/custom/plot_layout.js"></script>

<!--<div class="container">
    <div class="row">
        <div class="col-sm-12">
            <div>
                <ol class="breadcrumb">
                    <li><a href="javascript:void(0);">Home</a></li>
                    <li><a href="javascript:void(0);">Report</a></li>
                    <li class="active">Village Plots</li>
                </ol>
            </div>
        </div>
    </div>
</div>-->

<div class="container">      
    <div class="container print_hide">
        <div class="row">
            <div class="col-sm-1 search">
                <a href="javascript:void(0);" class="btn btn-info">
                    <span class="glyphicon glyphicon-search"></span>
                </a>
            </div>
            <div class="col-sm-11">
                <div>
                    <ol class="breadcrumb">
                        <li><a href="<?= url('/') ?>">Home</a></li>                     
                        <li><a href="javascript:void(0);">Report</a></li>
                        <li class="active">Village Claims Plot</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
</div>

<style>
    .chosen_select_custom {
        background: #f0f0f0;
        border: 1px solid #ccc;
        border-radius: 3px;
        height: 30px !important;
        padding: 6px 0 2px;
        width: 250px;
    }

    .loadnum{
        border: 1px solid #00aa01;
        border-radius: 0 5px 5px 0;
        height: 48px;
        padding: 4px;
        width:70px;
        background:#00aa01;
        box-shadow: 0 2px 0 #007300;
        vertical-align: middle;
        color: #fff;
        font-size: 20px;
        text-shadow: 0 0 1px #000101;
    }

    .submit_btn_drop {
        display: inline-block;
        margin: 0 3px;
    }

    .submit_btn_drop a {
        vertical-align: middle;
        background: #00aa01;
        border-left: 1px solid #00aa01;
        border-right: 1px solid #034403;
        border-top: 1px solid #00aa01;
        border-bottom: 1px solid #00aa01;
        border-radius: 5px 0 0 5px;
        box-shadow: 0 2px 0 #007300;
        color: #fff;
        font-size: 20px;
        letter-spacing: 2px;
        padding: 11px 5px 11px 38px;
        text-decoration: none;
        text-shadow: 0 0 1px #000101;
        text-transform: uppercase;
        text-decoration: none;
        transition: all .5s ease-in-out;
    }
    .submit_btn_drop a:hover {
        background: #007800;
        border: 1px solid #007800;
        text-shadow: none;
        text-decoration: none;
        color: #fff;
    }

</style>
<!-- breadcrumb ends  -->
<script>
    function populateDistrict(state_id) {
        $('.display_panel_content').css('cursor', 'wait');
        $('#district').children(':not(:first-child)').remove();
        $.get(serviceUrl + 'ajax-districts/' + state_id, function (data) {
            $.each(data, function (index, value) {
                $('#district').append('<option  value="' + $(this)[0].Id + '" >' + $(this)[0].Name + '</option>')
            });
//            $('#district').val('3');
        }, 'json').done(function () {
            $('.display_panel_content').css('cursor', '');
        });
        //$('#district').css('cursor', '');
    }
    function populateBlock(district_id) {
        $('.display_panel_content').css('cursor', 'wait');
        $('#block').children(':not(:first-child)').remove();
        $.get(serviceUrl + 'ajax-blocks/' + district_id, function (data) {
            $.each(data, function (index, value) {
                $('#block').append('<option  value="' + $(this)[0].Id + '" >' + $(this)[0].Name + '</option>')
            });
//             $('#block').val();
        }, 'json').done(function () {
            $('.display_panel_content').css('cursor', '');
        });

    }
    function populateVillage(block_id) {
        $('.display_panel_content').css('cursor', 'wait');
        $('#village').children(':not(:first-child)').remove();
        $.get(serviceUrl + 'ajax-villages/' + block_id, function (data) {
            $.each(data, function (index, value) {
                $('#village').append('<option  value="' + $(this)[0].Id + '" >' + $(this)[0].Name + '</option>')
            });
        }, 'json').done(function () {
            $('.display_panel_content').css('cursor', '');
        });
    }
    function populateClaims(village_id) {
        $('.display_panel_content').css('cursor', 'wait');
        $('#claim').children(':not(:first-child)').remove();
        $.get(serviceUrl + 'ajax-claim-under-village/' + village_id, function (data) {
            $.each(data, function (index, value) {
                $('#claim').append('<option  value="' + $(this)[0].Id + '" >' + $(this)[0].Name + '</option>');
            });
            setTimeout(function () {
                $(".chosen-select").chosen();
            }, 500);
        }, 'json').done(function () {
            $('.display_panel_content').css('cursor', '');
        });
    }
    function populateClaimant(village_id) {
        $('#claimant').css('cursor', 'wait');
        $('#claimant').children(':not(:first-child)').remove();
        $.get(serviceUrl + 'ajax-claimant-under-village/' + village_id, function (data) {
            $.each(data, function (index, value) {
                $('#claimant').append('<option  value="' + $(this)[0].Id + '" >' + $(this)[0].Name + '</option>');
            });
            setTimeout(function () {
                $(".chosen-select").chosen();
            }, 500);
        }, 'json').done(function () {
            $('.display_panel_content').css('cursor', '');
        });
        //choosen         

        //choosen 
    }
</script>
<!-- togglt bar starts  -->
<div class="toggle_bar_container">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 no_padding">
                <div class="toggle_bar">
                    <div class="toggle_bar_left">Search Plot....</div>
                    <div class="toggle_bar_right pull-right">
                        <a ng-show="search" id="view_panel" ng-click="search = false" onclick="$('#view_panel').hide(600, function () {
                                    $('#hide_panel').show();
                                    $('#display_panel').show();
                                })" style="display: none;" ><img src="images/down.png" alt="" /></a>
                        <a ng-hide="search" id="hide_panel" ng-click="search = true" onclick="$('#hide_panel').hide(600, function () {
                                    $('#view_panel').show();
                                    $('#display_panel').hide();
                                })" ><img src="images/up.png" alt="" /></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<!-- togglt bar ends  -->

<!-- togglt panel starts  -->
<form id="regionNav" >
    <div class="display_panel_container" id="display_panel" >
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="display_panel_content">

                        <div class="dropdown_container1">
                            <div class="custom_dropdown">
                                <div class="normal_text">State<span>*</span></div>
                                <div class="select-style">
                                    <select id="state" onchange="populateDistrict($(this).val());"  > <!--ng-options="state.name for state in states | orderBy : 'name' "-->
                                        <option value="">Select State</option>
                                    </select>
                                </div>
                            </div>
                            <div class="custom_dropdown">
                                <div class="normal_text">District<span>*</span></div>
                                <div class="select-style">
                                    <select id="district" onchange="populateBlock($(this).val())"   > <!--ng-options="district.name for district in districts | orderBy : 'name' "-->
                                        <option value="">Select District</option>
                                    </select>
                                </div>
                            </div>
                            <div class="custom_dropdown">
                                <div class="normal_text">Block/Tehsil<span>*</span></div>
                                <div class="select-style">
                                    <select id="block" onchange="populateVillage($(this).val())">
                                        <option value="">Select Block/Tehsil</option>
                                    </select>
                                </div>
                            </div>
                            <div class="custom_dropdown">
                                <div class="normal_text">
                                    <!--<button type="button" class="btn btn-primary" data-toggle="modal" data-target=".bs-example-modal-sm2" onclick="$('#vilcode').val($('#village').val())">Get Village Code</button><button type="button" class="btn btn-primary" data-toggle="modal" data-target=".bs-example-modal-sm">Add Village</button>-->
                                    Village<span>*</span></div>
                                <div class="select-style">
                                    <select id="village" onchange="populateClaims($(this).val());
                                            populateClaimant($(this).val())">
                                        <option value="Select Village">Select Village</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="dropdown_container2">
                            <div class="custom_dropdown">
                                <div class="normal_text">Claimant ID</span></div>
                                <div class=""> 
                                    <select name="claimant_id[]" id="claimant" class="chosen-select chosen_select_custom" multiple>
                                        <option value="">Select Claimant ID</option>
                                    </select>
                                </div>
                            </div>
                            <div class="custom_dropdown">
                                <div class="normal_text">Claim No.</div>
                                <div class="">
                                    <select id="claim" name="claim[]" class="chosen-select chosen_select_custom" multiple>
                                        <option value="">Select Claim No.</option>
                                    </select>
                                </div>
                            </div>
                            <p>ghjgjhghjgjhgjhgjghj</p>
                            <!--                            <div class="custom_dropdown">
                                                            <div>
                                                                <div class="normal_text">Claimant ID<span>*</span></div>
                                                                <div>
                                                                    <input class="input_text chosen-select" type="text" placeholder="Claimant ID" />
                                                                </div>
                                                            </div>
                                                        </div>-->
                        </div>

                        <div class="button_group">
                            <div class="load-msg" style="display: none">Loaded: <span class="loaded-plots"></span> of <span class="total-plots"></span></div>
                            <!--                            <div class="submit_btn"><a href="javascript:populatePlot();">submit</a></div> -->
                            <div class="submit_btn_drop load_btn_box"  ><a id="load-btn" onclick="load();" >Load</a><select class="loadnum" id="loadRec"><option value="0">All</option><option value="1" >1</option><option value="10" selected="true">10</option><option value="20">20</option></select></div> 
                            <!--                            <div class="submit_btn_drop"  ><a onclick="typeof (plotIds) == 'undefined' ? alert('Please Submit') : loadUntil += parseInt($('#loadRec').val());
                                                                loadMapRecursively(loadcount);" >Load</a><select class="loadnum" id="loadRec"><option value="0">All</option><option value="1" >1</option><option value="10" >10</option><option value="20">20</option></select></div> -->
                            <div class="reset_btn stop_load_box" style="display: none;"><a  href="javascript:void(0);" onclick="stop_load = true;">stop loading</a></div>
                            <div class="reset_btn" id="reset"><a  href="javascript:void(0);" onclick="resetForm();" >reset</a></div>

                        </div>

                        <div id="loader" class="loader" style="display: none;"><img src="<?= base_url() ?>images/loader.gif" /></div>


                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
</form>
<script>
    var plotIds = null;
    var plotIdLen = 0;
    var curIndex = 0;
    var loadcount = 0;
    var loadUntil = 0;

    function loadMapRecursively(index) {
        //put plotID specific html template
        //initialize plotid specific map

        $('#loader').show();
        console.log(index);
        $.post(serviceUrl + 'ajax-get-plotlayout/' + plotIds[index], {vilName: $('#village :selected').text(), state: $('#state :selected').text(), district: $('#district :selected').text(), block: $('#block :selected').text()}, function (data) {
            $('#plotAppend').append(data);
            if (index > loadUntil) {
//            if(index >10 ){
                //stop loading
                $('#loader').hide();
                return;

            } else {
                loadcount++;
                curIndex = parseInt(index) + 1;
                loadMapRecursively(curIndex);
            }
        }).done(function () {
            $('#loader').hide();
        });
    }
    function populatePlot() {
        if ($('#village :selected').text() == 'Select Village')
        {
            alert('Please select village');
            return;
        }

        var claimant_ids = [];
        $.each($("#claimant option:selected"), function () {
            claimant_ids.push($(this).val());
        });
        $('#plotAppend').html('');
        if ($('#loadRec').val() == '0')
            $.post(serviceUrl + 'ajax-get-plots-claim-village', {vilid: $('#village').val(), claimant_ids: claimant_ids}, function (data) {
                plotIds = data.plotIdList.split(',');
                plotIdLen = plotIds.length - 1;
                $('#loadRec').children(':first').val(plotIds.length);
////////////            loadMapRecursively(0);
//            updatePlotGeomRecursively(0);
//            for (plot in plotIds) {
//                //update geometry recursively
//
//                $.post(serviceUrl + 'ajax-get-plotlayout/' + plotIds[plot], {vilName: $('#village :selected').text(), state: $('#state :selected').text(), district: $('#district :selected').text(), block: $('#block :selected').text()}, function (data) {
//                    $('#plotAppend').append(data);
//                    $.get(serviceUrl + 'plotgpsGeoJSON/' + plotIds[plot], function () {
//
//                    })
////                    loadMap(plotIds[plot]);
//                })
//                if(plot == 1)return;
//            }

            }, 'json')
        else {

        }
        //update geometry recursively
        //once done start rendering maps recursively
    }

//    function updatePlotGeomRecursively(index) {
//        $.post(serviceUrl + 'UpdatePlotGeometry/' + plotIds[index], function (data) {
//
//
//            plotIdLen == index ? startMapRendering() : updatePlotGeomRecursively(index + 1);
//        })
//
//    }

//    function loadMap(plot) {
//        var map = new ol.Map({
//            target: 'map'+plot,
//            layers: [],
//            view: new ol.View({
//                projection: 'EPSG:4326',
////                    center: [73.80845276, 21.67167644],
//                center: [0, 0],
//                zoom: 10,
//                maxResolution: 0.703125
//            }),
//            controls: ol.control.defaults().extend([
//                new ol.control.ScaleLine()
//            ])
//        });
//        var url = 'http://192.168.1.47:8008/mapguide/RTP/plotgpsGeoJSON/'+plot;
//        var plotSourceGPS = new ol.source.Vector({
//            url: url,
//            format: new ol.format.GeoJSON({
//                defaultDataProjection: 'EPSG:4326',
//                projection: 'EPSG:3857'
//            })
//        });
//        var plotGPS = new ol.layer.Vector({
//            name: 'PlotGPS',
//            style: createPolygonStyleFunction
//        });
//        plotGPS.setSource(plotSourceGPS);
//        map.addLayer(plotGPS);
//    }

//    function setUnset(which, setflag) {
//        $.post(serviceUrl, {area: which, rtpflag: setflag}, function (data) {
//
//        })
//    }

    var last_loded_plot_index = 0;
    var stop_load = false;
    var total_loaded_plots = 0;

    function load() {
        //total_loaded_plots = 0;
        stop_load = false;
        if (plotIds == null) { ///ist time load
            if ($('#village :selected').text() == 'Select Village')
            {
                alert('Please select village');
                return;
            }

            var claimant_ids = [];
            $.each($("#claimant option:selected"), function () {
                claimant_ids.push($(this).val());
            });
            var claim_no = [];
            $.each($("#claim option:selected"), function () {
                claim_no.push($(this).val());
            });
            $('#plotAppend').html('');
            $.post(serviceUrl + 'ajax-get-plots-claim-village', {vilid: $('#village').val(), claimant_ids: claimant_ids, claim_no: claim_no}, function (data) {
                plotIds = data.plotIdList.split(',');
                plotIdLen = plotIds.length - 1;
                //alert(plotIdLen);
                $('#loadRec').children(':first').val(plotIds.length);
                initiatePlotLoading();
            }, 'json');
        } else { // 2nd time onwards load
            initiatePlotLoading();
        }
    }

    function initiatePlotLoading() { //no. of plots to load,index of last loaded plot
        var no_plot_load = parseInt($('#loadRec').val());
        $('#loader').show();
        $('#reset').css('display', 'none');
        //$('#load-btn').css('cursor', 'not-allowed');
        $('.load_btn_box').hide();
        $('.stop_load_box').css('display', 'inline');
        $('.load-msg').css('display', 'inline');
        // var total_plots_to_be_loaded = $('#loadRec option:selected').val();
        $('.total-plots').text(plotIds.length);
        createPlotHtmlRecursive(last_loded_plot_index, last_loded_plot_index + no_plot_load);
    }

    function createPlotHtmlRecursive(currentIndex, loadUpTo) {
        if (typeof (plotIds[currentIndex]) === 'undefined') {
            alert('No more plot to load !!');
        }
        if (currentIndex < loadUpTo && stop_load == false && typeof (plotIds[currentIndex]) !== 'undefined') {
            $.post(serviceUrl + 'ajax-get-plotlayout-old/' + plotIds[currentIndex], {vilName: $('#village :selected').text(), state: $('#state :selected').text(), district: $('#district :selected').text(), block: $('#block :selected').text()}, function (data) {
                $('#plotAppend').append(data);
                console.log(data)
                $('.loaded-plots').text(++total_loaded_plots);

                last_loded_plot_index++;
                createPlotHtmlRecursive(currentIndex + 1, loadUpTo);
//                total_loaded_plots++;
            })
        } else {
            $('#reset').css('display', 'inline');
            $('#loader').hide();
            $('.load_btn_box').css('display', 'inline');
            $('.stop_load_box').css('display', 'none');
            return false;
        }
    }

    function resetForm() {
        $('#regionNav')[0].reset();
        plotIds = null;
        $('#plotAppend').html('');
        console.log(plotIds);
        last_loded_plot_index = 0;
        $(".chosen-select ").chosen("destroy");
        $('#claimant').children(':not(:first-child)').remove();
//        stop_load = false;
        total_loaded_plots = 0;
        $('.load-msg').hide();
    }
    $(document).ready(function () {
        //$('.stop_load_box').hide();
        //alert(plot_layout_html);
        //$('#plotAppend').append(plot_layout_html);
        $.get(serviceUrl + 'ajax-states', function (data) {
            $.each(data, function (index, value) {
                $('#state').append('<option oncontextmenu="alert($(this).val());" value="' + $(this)[0].Id + '" >' + $(this)[0].Name + '</option>')
                //             alert(JSON.stringify($(this)[0]));
            });
        }, 'json');
    })
</script>
<!-- togglt panel ends  -->

<div class="plot_map_container">
    <div class="container" id="plotAppend">

    </div>
</div>