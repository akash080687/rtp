<div class="row" id="plot">
    <div class="col-sm-12">
        <div class="plot_map">
            <h1>Corrected Plot Report</h1>
            <div class="gray_border">
                <div class="common_section">
                    <h2>GPS Plot Detail </h2>
                    <div class="common_content1">
                        <div class="row">
                            <div class="col-sm-6 col-xs-6 col-md-6 col-lg-6">
                                <div class="table_container">
                                    <table>
                                        <tr>
                                            <td>State11</td>
                                            <td>:</td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td>Tehsil</td>
                                            <td>:</td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td>Plot No.</td>
                                            <td>:</td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td>Claim No.</td>
                                            <td>:</td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td>Claiment ID...</td>
                                            <td>:</td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td>Claiment Name</td>
                                            <td>:</td>
                                            <td></td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                            <div class="col-sm-6 col-xs-6 col-md-6 col-lg-6">
                                <div class="table_container">
                                    <table>
                                        <tr>
                                            <td>District</td>
                                            <td>:</td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td>Resident Village</td>
                                            <td>:</td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td>Plot Village</td>
                                            <td>:</td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td>Claim Village</td>
                                            <td>:</td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td>GPS Area in Hectares</td>
                                            <td>:</td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td>Satellite Area in Hectares</td>
                                            <td>:</td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td>Spouse Name</td>
                                            <td>:</td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="common_section">                  
                    <div class="common_content2">
                        <div id="map" class="map" style="padding: 0 5px;"></div>
                    </div>
                </div>
                <div class="common_section">
                    <h2>Set Of Coordinates --</h2>
                    <div class="common_content1">
                        <div class="row">
                            <div class="col-sm-4 col-xs-4 col-md-4 col-lg-4 box_adjust">
                                <div class="cord_table">
                                    <table>
                                        <tr>
                                            <th>No.</th>
                                            <th>Latitude</th>
                                            <th>Longitude</th>
                                            <th>No.</th>
                                            <th>Latitude</th>
                                            <th>Longitude</th>
                                            <th>No.</th>
                                            <th>Latitude</th>
                                            <th>Longitude</th>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>