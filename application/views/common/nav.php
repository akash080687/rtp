<!--<div class="navbar_container">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <nav class="navbar navbar-default">
                            <div class="container-fluid">
                                <div class="navbar-header">
                                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                                        <span class="sr-only">Toggle navigation</span>
                                        <span class="icon-bar"></span>
                                        <span class="icon-bar"></span>
                                        <span class="icon-bar"></span>
                                    </button>
                                </div>

                                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                                    <ul class="nav navbar-nav">
                                        <li><a href="#/home" >Home</a></li>
                                        <li><a href="javascript:void(0);">About Us</a></li>
                                        <li><a href="javascript:void(0);">Support Us</a></li>
                                        <li><a href="javascript:void(0);">Contact</a></li>
                                        <li class="active"><a href="#/viewPlot">View Plot</a></li>
                                        <li><a href="javascript:void(0);">Sample Reports</a></li>
                                        <li><a href="javascript:void(0);">Photo Gallery</a></li>
                                        <li><a href="javascript:void(0);">Blog</a></li>
                                    </ul>

                                    <ul class="nav navbar-nav navbar-right">
                                        <li class="right_bar"><a href="javascript:void(0);">Login</a></li>
                                        <li><a href="javascript:void(0);">Register</a></li>
                                    </ul>
                                </div>
                            </div>
                        </nav>
                    </div>
                </div>
            </div>
        </div>-->
<nav class="navbar navbar-default menubar">
    <div class="container">
        <div class="row">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </div>

            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li class="<?= (isset($page) && $page == 'home') ? 'active' : '' ?>"><a href="http://righttoproperty.local" >Home</a></li>
                    <li class="<?= (isset($page) && $page == 'about_us') ? 'active' : '' ?>"><a href="http://righttoproperty.local/about-us" >About Us</a></li>
                    <li class="<?= (isset($page) && $page == 'support_us') ? 'active' : '' ?>"><a href="http://righttoproperty.local/support-us" >Support Us</a></li>
                    <li class="<?= (isset($page) && $page == 'contact') ? 'active' : '' ?>"><a href="http://righttoproperty.local/contact" >Contact</a></li>
                    <li class="<?= (isset($page) && $page == 'view_plots') ? 'active' : '' ?>"><a href="http://righttoproperty.local/view-plots" >View Plot</a></li>

                    <!--                <li class=""><a href="#/viewPlot">View Plot</a></li>-->
                   
                        <li class="dropdown">
                            <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown">Reports <span class="caret"></span></a>
                            <ul class="dropdown-menu">
                              <!--  <li class=""><a href="http://righttoproperty.local/RTP/report/villge-claims-plot"> Village Claims Report</a></li>-->
                                <li class="<?= (isset($section) && $section == 'villge-claims-plot') ? 'active' : '' ?>"><a href="http://righttoproperty.local/report/villge-claims-plot">Village Claims Report</a></li><!--Faiz  
                                <li class=""><a href="http://righttoproperty.local/RTP/villge-claims-plot"> Village Claims Report</a></li>-->
                                <li class=""><a  href="http://righttoproperty.local/RTP/report-village-plot">Village Plots</a></li>

                            </ul>

                        </li> 
                    
                        <li class="dropdown">
                            <a href="javascript:void(0);" class="dropdown-toggle <?= (isset($page) && $page == 'manage_records') ? 'active' : '' ?>" data-toggle="dropdown">Manage Records <span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li class="<?= (isset($section) && $section == 'family') ? 'active' : '' ?>"><a href="http://righttoproperty.local/family/list">Family</a></li>
                                <li class="<?= (isset($section) && $section == 'village') ? 'active' : '' ?>"><a href="http://righttoproperty.local/village/list">Village</a></li>
                                <li class="<?= (isset($section) && $section == 'claimant') ? 'active' : '' ?>"><a href="http://righttoproperty.local/claimant/list">Claimant</a></li>
                                <li class="<?= (isset($section) && $section == 'claim') ? 'active' : '' ?>"><a href="http://righttoproperty.local/claim/list">Claim</a></li>
                                <li class="<?= (isset($section) && $section == 'plot') ? 'active' : '' ?>"><a href="http://righttoproperty.local/plot/list">Plot</a></li>
                            </ul>

                        </li>

                        <li class="dropdown">
                            <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown">Import/Export<span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li class="<?= (isset($section) && $section == 'import-excel') ? 'active' : '' ?>"><a href="http://righttoproperty.local/import/survey-data">Import Survey Data</a></li>                            
                            </ul>
                        </li>
                  
                    <li><a href="javascript:void(0);">Photo Gallery</a></li>
                    <li><a target="_blank" href="http://www.righttoproperty.org/blog/">Blog</a></li>
                    <li><a target="_blank" href="http://www.righttoproperty.org/map/">News Map</a></li>
                </ul>
                <!--            <a class="btn btn-primary" data-toggle="modal" href="#myModal" >Login</a>-->
                <ul class="nav navbar-nav navbar-right">
                  
                        <li class="right_bar"><a href="http://righttoproperty.local/logout">Logout</a></li>    
                   
                </ul>
            </div>
        </div>
    </div>
</nav>



