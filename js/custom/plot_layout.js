var plot_layout_html = '';
var plot_coordinate_section = '';
plot_layout_html =
    '<div class="row">' +
    '<div class="col-sm-12">' +
    '<div class="plot_map">' +
    //'<h1>Corrected Plot Reports__Faiz<span class="custom-page-num"></span></h1>' +
    '<h1>Corrected Plot Reports.<span class="custom-page-num"></span></h1>' +
    '<div class="gray_border">' +
    '<div class="main_page_break_container"> <div class="common_section">' +
    //'<h2>GPS Plot Details___Faiz <div class="pull-right">' +
    '<h2>GPS Plot Details<div class="pull-right">' +
    '<a class="reload_plot" href="javascript:void(0);">\n\
                        <img alt="Reload" title="Reload" src="images/reload.png"></a>' +
    '<a class="plot_kml_download" href="javascript:void(0);" target="_blank" ><img alt="KML" title="Download KML." src="images/google-earth.png"></a>' +
    '</div></h2>' +
    '<input type="hidden" value="" class="reload_plot_index">\
                        <div class="common_content1">' +
    '<div class="row">' +
    '<div class="col-sm-6 col-xs-6 col-md-6 col-lg-6">' +
    '<div class="table_container">' +
    '<table>' +
    '<tr>' +
    '<td>State</td>' +
    '<td>:</td>' +
    '<td class="state"></td>' +
    '</tr>' +
    '<tr>' +
    //'<td>Tehsil __faiz</td>' +
    '<td>Tehsil</td>' +
    '<td>:</td>' +
    '<td class="tehsil"></td>' +
    '</tr>' +
    '<tr>' +
    '<td>Plot No.</td>' +
    '<td>:</td>' +
    '<td class="plot-no"></td>' +
    '</tr>' +
    '<tr>' +
    '<td>Claim No.</td>' +
    '<td>:</td>' +
    '<td class="claim-no"></td>' +
    '</tr>' +
    '<tr>' +
    '<td>Claiment ID</td>' +
    '<td>:</td>' +
    '<td class="claiment-id"></td>' +
    '</tr>' +
    '<tr>' +
    '<td>&nbsp;</td>' +
    '<td>&nbsp;</td>' +
    '<td>&nbsp;</td>' +
    '</tr>' +
    '<tr>' +
    '<td>Claiment&nbsp;Name</td>' +
    '<td>:</td>' +
    '<td class="claiment-name"></td>' +
    '</tr>' +
    '</table>' +
    '</div>' +
    '</div>' +
    '<div class="col-sm-6 col-xs-6 col-md-6 col-lg-6">' +
    '<div class="table_container">' +
    '<table>' +
    '<tr>' +
    '<td>District</td>' +
    '<td>:</td>' +
    '<td class="district"></td>' +
    '</tr>' +
    '<tr>' +
    '<td>Resident&nbsp;Village</td>' +
    '<td>:</td>' +
    '<td class="res-village"></td>' +
    '</tr>' +
    '<tr>' +
    '<td>Plot&nbsp;Village</td>' +
    '<td>:</td>' +
    '<td class="plot-village"></td>' +
    '</tr>' +
    '<tr>' +
    '<td>Claim&nbsp;Village</td>' +
    '<td>:</td>' +
    '<td class="claim-village"></td>' +
    '</tr>' +
    '<tr>' +
    '<td>GPS Area &nbsp;&nbsp;&nbsp;<img src="http://righttoproperty.local/images/dotarrow_notation.png"></td>' +
    '<td>:</td>' +
    '<td class="gps-area"></td>' +
    '</tr>' +
    '<tr>' +
    '<td>Satellite&nbsp;Area&nbsp;&nbsp;<img src="http://righttoproperty.local/images/arrow_notation.png"></td>' +
    '<td>:</td>' +
    '<td class="sat-area"></td>' +
    '</tr>' +
    '<tr>' +
    '<td>Spouse Name</td>' +
    '<td>:</td>' +
    '<td class="spouse-name"></td>' +
    '</tr>' +
    '</table>' +
    '</div>' +
    '</div>' +
    '</div>' +
    '</div>' +
    '</div>' +
    '<div class="common_section sec2"> ' +
    '<div class="common_content2">' +
    '<div id="map" class="map" style="padding: 0 5px;"></div>' +
    '</div>' +
    '</div>' +
    '</div>';


//<a class="view_old_url" target="_blank" href="">View Old</a>
//<a class="reload_plot" href="javascript:void(0);"  current_index="" onclick="reloadSinglePlotHtml($(this).attr(\'current_index\'))" >Reload</a>